package com.hiva.flutter.server

import android.content.Context
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.server.db.FlutterDB
import com.hiva.flutter.server.db.FlutterDao
import com.hiva.flutter.server.db.entry.DataAlarm
import com.hiva.flutter.server.db.entry.DataFlutter

/**
 *
 * Create By HuangXiangXiang 2022/4/13
 * 本地服务
 */
class DBHelper private constructor(context: Context) {


    companion object{

        @Volatile
        private var INSTANCE: DBHelper? = null

        fun getInstance(context: Context): DBHelper =
            INSTANCE?: synchronized(this){
                INSTANCE?: DBHelper(context.applicationContext) .also { INSTANCE = it }
            }
    }

    private val flutterDao: FlutterDao = FlutterDB.getInstance(context).flutterDao()


    /**增加任务*/
    fun insertOrUpdateFlutter(dataFlutter: DataFlutter): Boolean{

        val list = flutterDao.queryFlutter(dataFlutter.id)
        val result: Boolean = if (list.isEmpty()) { // 不存在
            dataFlutter.createTime = System.currentTimeMillis()
            val id = flutterDao.insertFlutter(dataFlutter)
            dataFlutter.id = id
            true
        } else {
            val change = flutterDao.updateFlutter(dataFlutter)
            change > 0
        }
        return result
    }

    /**删除任务*/
    fun deleteFlutter(id: Long): Boolean{

        val dataFlutter = DataFlutter(id)
        val result = flutterDao.deleteFlutter(dataFlutter) > 0

        return result
    }


    /**
     * 查询任务
     * */
    fun queryFlutter(id: Long) : DataFlutter?{

        val list = flutterDao.queryFlutter(id)
        val result: DataFlutter? = if(list.isNotEmpty()){list[0]}else{null}
        return result
    }


    /**
     * 查询全部任务
     * */
    fun queryFlutters(requestFilters: RequestFilters): List<DataFlutter> {

        val list = flutterDao.queryFlutters(requestFilters)
        return list
    }


    /**
     * 查询全部活跃的
     * */
    fun queryAliveFlutters(): MutableList<DataFlutter> {

        val list = flutterDao.queryAllAliveFlutters()
        return list
    }

    /**********************************************/
    /**修改闹钟*/
    fun insertAlarm(alarm: DataAlarm): Boolean{

        val id = flutterDao.insertAlarm(alarm)
        alarm.id = id

        return id > 0
    }

    /**修改闹钟*/
    fun readAlarm(id: Long): Boolean{

        val change = flutterDao.readAlarm(id)
        return change > 0
    }

    /**修改闹钟*/
    fun readAllAlarm(): Boolean{

        val change = flutterDao.readAllAlarm()
        return true
    }


    /**
     * 查询任务
     * */
    fun queryUnReadAlarm() : List<DataAlarm>{

        val result = flutterDao.queryAllUnReadAlarm()
        return result
    }

    /**
     * 查询闹钟数量
     * */
    fun queryUnReadAlarmSize() : Int{

        val result = flutterDao.queryAllUnReadAlarmSize()
        return result
    }




}