package com.hiva.flutter.server

import android.content.Context
import com.hiva.ICommunication
import com.hiva.flutter.CommunicationConstant
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.response.Response
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.helper.json.JsonUtils
import com.hiva.helper.log.LogHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/16
 */
class From(val from: String, context: Context) {

    private val function = ServerFlutterFunction(context)

     fun request(
        type: String,
        json: String?,
        listener: ICommunication.OnResponseListener?
    ) {
        LogHelper.i("$type : $json")

        when (type) {

            CommunicationConstant.TOPIC_ALIVE_FLUTTERS -> {

                listener?.let{
                    function.setAliveFlutterChangedListener(transformListener(it))
                }?: run {
                    function.setAliveFlutterChangedListener(null)
                }
            }
            CommunicationConstant.REQUEST_QUERY_ALIVE_FLUTTERS -> {
                listener?.let {
                    function.queryAliveFlutters(transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_QUERY_SORT_NAMES -> {
                listener?.let {
                    function.querySortNames(transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_QUERY_FILTER_NAMES -> {
                listener?.let {
                    function.queryFilterNames(transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_QUERY_FLUTTERS -> {
                listener?.let {
                    val requestFilters = JsonUtils.parseJson(json, RequestFilters::class.java)
                    function.queryFlutters(requestFilters, transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_INSERT_OR_UPDATE_FLUTTER -> {
                listener?.let {
                    val flutterDetail = JsonUtils.parseJson(json, FlutterDetail::class.java)
                    function.insertOrUpdateFlutter(flutterDetail, transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_DELETE_FLUTTER -> {
                listener?.let {
                    val id: Long = JsonUtils.parseJson(json, Long::class.java)
                    function.deleteFlutter(id, transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_QUERY_DETAIL_FLUTTER -> {
                listener?.let {
                    val id: Long = JsonUtils.parseJson(json, Long::class.java)
                    function.queryDetailFlutter(id, transformListener(it))
                }
            }
            CommunicationConstant.TOPIC_ALARM -> {

                listener?.let{
                    function.setAlarmChangedListener(transformListener(it))
                }?: run {
                    function.setAlarmChangedListener(null)
                }
            }
            CommunicationConstant.REQUEST_QUERY_UN_READ_ALARM -> {

                listener?.let{
                    function.queryUnReadAlarm(transformListener(it))
                }
            }

            CommunicationConstant.REQUEST_QUERY_UN_READ_ALARM_SIZE -> {

                listener?.let{
                    function.queryUnReadAlarmSize(transformListener(it))
                }
            }

            CommunicationConstant.REQUEST_READ_ALARM -> {

                listener?.let{

                    val id: Long = JsonUtils.parseJson(json, Long::class.java)
                    function.readAlarm(id, transformListener(it))
                }
            }
            CommunicationConstant.REQUEST_READ_ALL_ALARM -> {

                listener?.let{
                    function.readAllAlarm(transformListener(it))
                }
            }
        }
    }


    /***/
    private fun <T> transformListener(listener: ICommunication.OnResponseListener): OnResponseListener<T> {

        return object : OnResponseListener<T> {
            override fun onSuccess(t: T) {

                val response = Response.success(t)
                listener.onResponse(JsonUtils.toJson(response))
            }

            override fun onFail(code: Int, message: String?) {

                val response = Response.fail<T>(code, message)
                listener.onResponse(JsonUtils.toJson(response))
            }
        }

    }

}