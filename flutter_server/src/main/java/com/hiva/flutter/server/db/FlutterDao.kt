package com.hiva.flutter.server.db

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import com.hiva.flutter.bean.Filter
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.server.db.entry.DataAlarm
import com.hiva.flutter.server.db.entry.DataFlutter
import com.hiva.helper.log.LogHelper
import java.lang.Exception
import java.lang.StringBuilder

/**
 *
 * Create By HuangXiangXiang 2022/2/14
 *
 *
 */
@Dao
interface FlutterDao {


    /**
     * 增加提醒
     * */
    @Insert
    fun insertFlutter(dataFlutter: DataFlutter): Long

    /**
     * 删除提醒
     * */
    @Delete
    fun deleteFlutter(dataFlutter: DataFlutter): Int

    /**
     * 修改 提醒
     * */
    @Update
    fun updateFlutter(dataFlutter: DataFlutter): Int

    /**
    * 查找提醒（根据要求去查询）
    * */
    @Query("select * from flutter where id=:id")
    fun queryFlutter(id: Long): List<DataFlutter>


    /**
     * 查找提醒（根据要求去查询）
     * */
//    @Query("select * from flutter where id=:id")
//    fun isExistFlutter(id: Long): Boolean


    /**
     * 查找提醒（根据要求去查询）
     * */
    fun queryFlutters(requestFilters: RequestFilters): List<DataFlutter>{

        val sb = StringBuilder("select * from flutter ")

        val list = requestFilters.list
        val size = list.size
        if (size > 0 ){
            sb.append("where ")

            var isFirst = true

            for (i in 0 until size){
                val filter = list[i]
                val sql = toSql(filter)
                sql?.let {

                    if(!isFirst) sb.append(" and ") // 不是第一次 追加连接

                    sb.append(it)
                    isFirst = false
                }
            }
        }
        val name = requestFilters.sort.name
        val orderBy = if(name == null){
            ""
        }else{
            if(requestFilters.sort.isUp){
                "order by " + name.type + " asc" //上升
            }else{
                "order by " + name.type + " desc"
            }
        }
        sb.append(orderBy)

        val sql = sb.toString()
        LogHelper.i(sql)

        val query: SupportSQLiteQuery = SimpleSQLiteQuery(sql)

        return queryFlutter(query)
    }


    /**自定义查询*/
    @RawQuery
    fun queryFlutter(query: SupportSQLiteQuery): List<DataFlutter>


    /**
     * 查询全部活跃的任务，并且按照时间倒序排列
     * */
    @Query("select * from flutter where state='TASK' ")
    fun queryAllAliveFlutters(): MutableList<DataFlutter>

    private fun toSql(filter: Filter):String?{

        when (filter.type) {
            Filter.Type.LESS -> {
                return "${filter.name.type} < ${toValue(filter.value)}"
            }
            Filter.Type.MORE -> {
                return "${filter.name.type} > ${toValue(filter.value)}"
            }
            Filter.Type.EQUAL -> {
                return "${filter.name.type} == ${toValue(filter.value)}"
            }
            Filter.Type.NOT_EQUAL -> {
                return "${filter.name.type} != ${toValue(filter.value)}"
            }
            Filter.Type.LIKE -> {
                return "${filter.name.type} like '%${filter.value}%'"
            }
            else -> {
                return null
            }
        }
    }


    private fun toValue(value:String):String{

        return try {
            value.toLong()
            value
        }catch (e : Exception){
            "'$value'"
        }

    }




    /**
     * 增加提醒
     * */
    @Insert
    fun insertAlarm(alarm: DataAlarm): Long

//    /**
//     * 删除提醒
//     * */
//    @Delete
//    fun deleteAlarm(id: Long): Int

//    /**
//     * 修改 提醒
//     * */
//    @Update
//    fun updateAlarm(alarm: DataAlarm): Int
//
//    /**
//     * 查找提醒（根据要求去查询）
//     * */
//    @Query("select * from alarm where id=:id")
//    fun queryAlarm(id: Long): List<DataAlarm>




    /**
     * 查询全部没有读取的
     * false 0
     * true 1
     * */
    @Query("select * from alarm where isRead = 0 order by time desc")
    fun queryAllUnReadAlarm(): List<DataAlarm>

    /**
     * 查询全部没有读取的数目
     * false 0
     * true 1
     * */
    @Query("select count(*) from alarm where isRead = 0")
    fun queryAllUnReadAlarmSize(): Int

    @Query("update alarm set isRead = 1 where id=:id")
    fun readAlarm(id: Long): Int

    /**
     * 全部已读
     * */
    @Query("update alarm set isRead = 1 where isRead = 0")
    fun readAllAlarm(): Int




}