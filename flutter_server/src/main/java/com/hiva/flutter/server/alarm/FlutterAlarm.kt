package com.hiva.flutter.server.alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.hiva.helper.log.LogHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/18
 */
class FlutterAlarm private constructor(val context: Context) {

    companion object{


        private const val ACTION_ALARM = "com.yongyida.robot.ALARM"

        private const val KEY_ID        = "id"
        private const val KEY_TIME      = "time"
        private const val KEY_TITLE     = "title"
        private const val KEY_INFO      = "info"


        @Volatile
        private var INSTANCE: FlutterAlarm? = null

        fun getInstance(context: Context): FlutterAlarm =
            INSTANCE?: synchronized(this){
                INSTANCE?: FlutterAlarm(context.applicationContext) .also { INSTANCE = it }
            }


    }

    private val alarmManager = context.getSystemService(android.app.Service.ALARM_SERVICE) as AlarmManager

    interface OnAlarmListener{

        fun onAlarm(id: Long, time: Long, title: String, info: String)
    }
    private var mOonAlarmListener: OnAlarmListener? = null

    private var broadcastReceiver: BroadcastReceiver? = null


    /**开始*/
    fun start(onAlarmListener: OnAlarmListener) {

       this.mOonAlarmListener = onAlarmListener
       this.broadcastReceiver?:run{

            broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {

                    val id = intent.getLongExtra(KEY_ID, -1)
                    val time = intent.getLongExtra(KEY_TIME, -1)
                    val title = intent.getStringExtra(KEY_TITLE)
                    val info = intent.getStringExtra(KEY_INFO)
                    LogHelper.i("id : $id")
                    if(id > -1 && title != null &&  info != null){

                        if (ACTION_ALARM == intent.action) {
                            mOonAlarmListener?.onAlarm(id, time, title, info)
                        }
                    }
                }
            }
            val filter = IntentFilter(ACTION_ALARM)
            context.registerReceiver(broadcastReceiver, filter)
        }
    }

    /**停止*/
    fun stop() {

        broadcastReceiver?.let {

            context.unregisterReceiver(broadcastReceiver)
            broadcastReceiver = null
        }
    }


    /**
     * 开始闹钟计时
     * */
    fun startTimerAlarm(id: Long, time: Long,title: String, info: String){

        val pendingIntent: PendingIntent = getPendingIntent(ACTION_ALARM, id, time, title, info)
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent)
    }

    /**
     * 结束闹钟计时
     * */
    fun stopTimerAlarm(id: Long){

        val pendingIntent: PendingIntent = getPendingIntent(ACTION_ALARM, id)
        alarmManager.cancel(pendingIntent)
    }


    private fun getPendingIntent(action : String, id: Long, time: Long? = null,  title : String? = null, info: String? = null): PendingIntent {

        val alarmIntent = Intent(action)
        alarmIntent.putExtra(KEY_ID, id)
        time?.let {
            alarmIntent.putExtra(KEY_TIME, it)
        }

        title?.let {
            alarmIntent.putExtra(KEY_TITLE, it)
        }

        info?.let {
            alarmIntent.putExtra(KEY_INFO, it)
        }

        return PendingIntent.getBroadcast(context, id.toInt(), alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
    }


}