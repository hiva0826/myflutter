package com.hiva.flutter.server.db.entry

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.hiva.flutter.bean.Alarm

/**
 *
 * Create By HuangXiangXiang 2022/8/9
 */
@Entity(tableName = "alarm")
data class DataAlarm(

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,               // id
    var fId : Long = 0,             // 提醒事件
    var time: Long = 0 ,            // 提醒时间
    var title : String = "" ,       // 提醒标题
    var info: String = "提醒到了",    // 提醒信息
    var isRead : Boolean = false ,  // 是否已看
){

    companion object{

        fun transform(alarm: Alarm):DataAlarm{

            return DataAlarm(
                alarm.id,
                alarm.fId,
                alarm.time,
                alarm.title,
                alarm.info,
                alarm.isRead,
            )
        }

        fun transform(alarm: DataAlarm): Alarm {

            return Alarm(
                alarm.id,
                alarm.fId,
                alarm.time,
                alarm.title,
                alarm.info,
                alarm.isRead,
            )
        }


        fun transformData(list: List<Alarm>): List<DataAlarm>{

            val transformList = ArrayList<DataAlarm>(list.size)
            for (data in list){
                transformList.add(transform(data))
            }
            return transformList
        }


        fun transform(list: List<DataAlarm>): List<Alarm> {

            val transformList = ArrayList<Alarm>(list.size)
            for (data in list){
                transformList.add(transform(data))
            }
            return transformList

        }


    }



}