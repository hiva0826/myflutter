package com.hiva.flutter.server.db.entry

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.FlutterDetail

/**
 * Create By HuangXiangXiang 2022/2/14
 *  代办事项
 */
@Entity(tableName = "flutter")
data class DataFlutter(

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,                                           // id
    var createTime: Long? = 0,                                  // 创建时间
    var title: String = "",                                     // 事情名称
    var describe: String?= null,                                // 事情的描述
    var level: Int? = 0,                                        // 重要等级（数字越大 越重要 默认为0）
    var state: FlutterDetail.State? = FlutterDetail.State.PLAN, // 状态
    var endTime: Long?  = null,                                 // 结束时间
    @Embedded var reminder: FlutterDetail.Reminder? = null,     // 提醒信息
    @Embedded var task: FlutterDetail.Task? = null,             // 事件内容
)  {

    companion object{

        fun transform(flutter : FlutterDetail):DataFlutter{

            return DataFlutter(
                flutter.id,
                flutter.createTime,
                flutter.title,
                flutter.describe,
                flutter.level,
                flutter.state,
                flutter.endTime,
                flutter.reminder,
                flutter.task,
                )
        }

        fun transform(flutter: DataFlutter):FlutterDetail{

            return FlutterDetail(
                flutter.id,
                flutter.createTime,
                flutter.title,
                flutter.describe,
                flutter.level,
                flutter.state,
                flutter.endTime,
                flutter.reminder,
                flutter.task,
            )
        }

        fun transform(list: List<DataFlutter>): List<Flutter>{

            val transformList = ArrayList<Flutter>(list.size)
            for (dataFlutter in list){
                transformList.add(transformFlutter(dataFlutter))
            }
            return transformList
        }

        fun transformFlutterDetails(list: List<DataFlutter>): List<FlutterDetail>{

            val transformList = ArrayList<FlutterDetail>(list.size)
            for (dataFlutter in list){
                transformList.add(transform(dataFlutter))
            }
            return transformList
        }

        fun transformFlutter(dataFlutter: DataFlutter): Flutter{
            return Flutter(dataFlutter.id, dataFlutter.title, dataFlutter.level, dataFlutter.endTime)
        }


    }




}