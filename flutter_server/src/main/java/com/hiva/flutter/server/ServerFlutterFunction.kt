package com.hiva.flutter.server

import android.content.Context
import com.hiva.flutter.FlutterFunction
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.Name
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.response.*
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.flutter.server.db.entry.DataAlarm
import com.hiva.flutter.server.db.entry.DataFlutter
import com.hiva.helper.log.LogHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/12
 */
class ServerFlutterFunction(context: Context): FlutterFunction {

    private val dbHelper : DBHelper = DBHelper.getInstance(context)

    private val flutterHandler : FlutterHandler = FlutterHandler(context, dbHelper)


    override fun setAliveFlutterChangedListener(listener: OnResponseListener<ResponseChangedAliveFlutter>?) {

        flutterHandler.aliveFlutterListener = listener
    }


    override fun queryAliveFlutters(listener: OnResponseListener<ResponseAliveFlutters>) {

        val list = flutterHandler.aliveFlutters
        LogHelper.i(list)
        val aliveFlutters = DataFlutter.transform(list)

        listener.onSuccess(ResponseAliveFlutters(aliveFlutters))
    }


    override fun querySortNames(listener: OnResponseListener<ResponseSortNames>) {

        val sortNames = ArrayList<Name>()
        sortNames.add(Name("createTime", "创建时间"))
        sortNames.add(Name("endTime", "结束时间"))
        sortNames.add(Name("level", "重要等级"))
        sortNames.add(Name("state", "状态"))
        val responseSortNames = ResponseSortNames(sortNames)

        listener.onSuccess(responseSortNames)
    }

    override fun queryFilterNames(listener: OnResponseListener<ResponseFilterNames>) {

        val names = ArrayList<Name>()
        names.add(Name("createTime", "创建时间"))
        names.add(Name("title", "标题"))
        names.add(Name("describe", "描述"))
        names.add(Name("endTime", "结束时间"))
        names.add(Name("level", "重要等级"))
        names.add(Name("state", "状态"))

        val responseFilterNames = ResponseFilterNames(names)

        listener.onSuccess(responseFilterNames)
    }

    /**根据过滤条件查询数据*/
    override fun queryFlutters(
        requestFilters: RequestFilters,
        listener: OnResponseListener<ResponseFlutters>
    ) {

        val list = dbHelper.queryFlutters(requestFilters)
        val result = DataFlutter.transformFlutterDetails(list)

        listener.onSuccess(ResponseFlutters(result))
    }

    /**增加或者修改*/
    override fun insertOrUpdateFlutter(
        flutterDetail: FlutterDetail,
        listener: OnResponseListener<ResponseBoolean>
    ) {

        val dataFlutter = DataFlutter.transform(flutterDetail)

        val result = dbHelper.insertOrUpdateFlutter(dataFlutter)
        listener.onSuccess(ResponseBoolean(result))

        flutterHandler.insertOrUpdateFlutter(dataFlutter)
    }

    /**删除*/
    override fun deleteFlutter(id: Long, listener: OnResponseListener<ResponseBoolean>) {

        LogHelper.i(id)

        val result = dbHelper.deleteFlutter(id)
        listener.onSuccess(ResponseBoolean(result))

        flutterHandler.deleteFlutter(id)
    }

    override fun queryDetailFlutter(id: Long, listener: OnResponseListener<ResponseFlutterDetail>) {

        val result = dbHelper.queryFlutter(id)

        val responseFlutterDetail : ResponseFlutterDetail = if(result != null){
            ResponseFlutterDetail(DataFlutter.transform(result))
        }else{
            ResponseFlutterDetail(null)
        }
        listener.onSuccess(responseFlutterDetail)
    }

    override fun setAlarmChangedListener(listener: OnResponseListener<ResponseChangedAlarm>?) {

        flutterHandler.alarmListener = listener
    }

    override fun queryUnReadAlarm(listener: OnResponseListener<ResponseUnReadAlarm>) {

        val list = dbHelper.queryUnReadAlarm()
        val result = DataAlarm.transform(list)

        listener.onSuccess(ResponseUnReadAlarm(result))
    }

    override fun queryUnReadAlarmSize(listener: OnResponseListener<ResponseUnReadAlarmSize>) {

        val result = dbHelper.queryUnReadAlarmSize()

        listener.onSuccess(ResponseUnReadAlarmSize(result))
    }


    override fun readAlarm(id: Long, listener: OnResponseListener<ResponseBoolean>) {

        val result = dbHelper.readAlarm(id)
        listener.onSuccess(ResponseBoolean(result))
    }

    override fun readAllAlarm(listener: OnResponseListener<ResponseBoolean>) {
        val result = dbHelper.readAllAlarm()
        listener.onSuccess(ResponseBoolean(result))
    }


}