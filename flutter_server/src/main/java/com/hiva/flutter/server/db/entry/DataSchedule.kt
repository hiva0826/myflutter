package com.hiva.flutter.server.db.entry

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *
 * Create By HuangXiangXiang 2022/2/15
 * 任务进度
 */
@Entity(tableName = "schedule")
data class DataSchedule(

    @PrimaryKey(autoGenerate = true)
    var id: Long ,
    var fid: Long  ,        // 任务id
    var info: String ,      // 更新内容
    var updateTime: Long ,  // 更新时间
) {




}