package com.hiva.flutter.server

import android.content.Context
import com.hiva.ICommunication
import com.hiva.flutter.bean.response.Response
import com.hiva.helper.json.JsonUtils
import com.hiva.helper.log.LogHelper
import java.util.*
import java.util.concurrent.Executors

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
class ServerCommunication(private val context: Context) : ICommunication {

    private val fromMap = HashMap<String, From>()

    private val single = Executors.newSingleThreadExecutor()

    override fun request(
        from: String,
        type: String,
        message: String?,
        listener: ICommunication.OnResponseListener?
    ) {
        LogHelper.i("$from : $type : $message")

        single.submit {

            try {
                var f = fromMap[from]
                if(f == null){
                    f = From(from, context)
                    fromMap[from] = f
                }
                f.request(type, message, listener)

            }catch (e : Exception){

                listener?.let{
                    val response = Response.fail<String>(Response.CODE_INNER, e.message)
                    it.onResponse(JsonUtils.toJson(response))
                }
            }
        }




    }

}