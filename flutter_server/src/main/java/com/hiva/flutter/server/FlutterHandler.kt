package com.hiva.flutter.server

import android.content.Context
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.response.ResponseChangedAlarm
import com.hiva.flutter.bean.response.ResponseChangedAliveFlutter
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.flutter.server.alarm.FlutterAlarm
import com.hiva.flutter.server.db.entry.DataAlarm
import com.hiva.flutter.server.db.entry.DataFlutter
import com.hiva.flutter.utils.HolidaysUtils
import com.hiva.flutter.utils.WeekUtils
import com.hiva.helper.log.LogHelper
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/7/18
 *
 * 代办事项处理类
 * 后台不做任何数据修改，为了让前端增加参与感
 *
 */
class FlutterHandler(context: Context, private val dbHelper : DBHelper) {

    companion object{

        private const val TIMER_NONE : Long = -1 // 无效定时数据
    }

    val aliveFlutters = dbHelper.queryAliveFlutters()

    private val flutterAlarm : FlutterAlarm = FlutterAlarm.getInstance(context)

    var alarmListener: OnResponseListener<ResponseChangedAlarm>? = null

    var aliveFlutterListener: OnResponseListener<ResponseChangedAliveFlutter>? = null

    init {

        val onAlarmListener: FlutterAlarm.OnAlarmListener = object : FlutterAlarm.OnAlarmListener{

            override fun onAlarm(id: Long, time: Long, title: String, info: String) {
                LogHelper.i(id)
                val index = queryDataFlutter(id)
                if(index > -1){
                    val flutter = aliveFlutters[index]
                    startTimer(flutter)
                }

                val dataAlarm = DataAlarm(0, id, time ,title, info)
                dbHelper.insertAlarm(dataAlarm)

                val alarm = DataAlarm.transform(dataAlarm)
                alarmListener?.onSuccess(ResponseChangedAlarm(alarm))
            }
        }
        flutterAlarm.start(onAlarmListener)

        for (flutter in aliveFlutters){
            startTimer(flutter)
        }
    }

    /**
     * 增加或者修改数据
     * */
    fun insertOrUpdateFlutter(dataFlutter: DataFlutter) {

        if(dataFlutter.state != FlutterDetail.State.TASK){ // 当前不在计划中（等于从列表中移除）
            deleteFlutter(dataFlutter.id) // 如果存在删除

        }else{ // 当前任务在计划中

            val index = queryDataFlutter(dataFlutter.id)
            if(index > -1) { // 之前有数据
                if(aliveFlutters[index] != dataFlutter){   // 数据不同

                    if(aliveFlutters[index].reminder != dataFlutter.reminder){ //提醒数据
                        startTimer(dataFlutter)
                    }
                    //替换数据
                    aliveFlutters[index] = dataFlutter
                }
            }else{// 之前没有数据

                startTimer(dataFlutter)
                aliveFlutters.add(dataFlutter)
            }

            aliveFlutterListener?.onSuccess(ResponseChangedAliveFlutter())
        }
    }


    /**
     * 删除数据
     * 通知删除数据
     * */
    fun deleteFlutter(id: Long) {

        val index = queryDataFlutter(id)
        if(index >= 0) {

            //停止闹钟
            stopTimer(id)

            aliveFlutters.removeAt(index) // 列表中移除

            //
            aliveFlutterListener?.onSuccess(ResponseChangedAliveFlutter())
        }
    }


    /**查询*/
    private fun queryDataFlutter(id : Long): Int{

        for (i in aliveFlutters.indices){
            if(id == aliveFlutters[i].id){
                return i
            }
       }
       return -1
    }


    private fun startTimer(flutter: DataFlutter){

        val alarm = flutter.nextTimer()
        if(alarm.time > 0){

            val date = Date( alarm.time)
            LogHelper.i(date.toLocaleString())

            flutterAlarm.startTimerAlarm(alarm.fId, alarm.time, alarm.title, alarm.info)
        }else{
            stopTimer(alarm.fId)
        }
    }

    private fun stopTimer(id: Long){

        flutterAlarm.stopTimerAlarm(id)
    }

    /**获取下一个定时*/
    private fun DataFlutter.nextTimer():DataAlarm{

        var dataAlarm = try{
            reminder?.nextTimer()
        }catch (e: Exception){
            null
        }

        if(dataAlarm == null){
            dataAlarm = DataAlarm(time = TIMER_NONE)
        }else{
            dataAlarm.title = title
        }

        dataAlarm.fId = id

        return dataAlarm
    }

    private fun FlutterDetail.Reminder.nextTimer(): DataAlarm?{

        if(reminderType == FlutterDetail.Reminder.Type.FIXED){ //固定

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)

            val date = simpleDateFormat.parse(reminderValue)
            if(date.time > System.currentTimeMillis()){ // 目标时间大于现在

                return DataAlarm(time = date.time)
            }

        }else if(reminderType == FlutterDetail.Reminder.Type.YEAR){ //每年

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)

            val date = simpleDateFormat.parse(reminderValue) //
            val now = Date()

            date.year = now.year

            if(date.time < now.time){ // 目标时间小于现在
                date.year += 1  // 加一年时间
            }
            return DataAlarm(time = date.time)

        }else if(reminderType == FlutterDetail.Reminder.Type.MONTH){ //每月（18 14:41:22）

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)

            val date = simpleDateFormat.parse(reminderValue) //
            val now = Date()

            date.year = now.year
            date.month = now.month

            if(date.time < now.time){ // 目标时间小于现在
                date.month += 1       // 加一个月时间
            }
            return DataAlarm(time = date.time)

        }else if(reminderType == FlutterDetail.Reminder.Type.DAY){ //每天 （14:41:22）

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)
            val date = simpleDateFormat.parse(reminderValue)
            val now = Date()

            date.year = now.year
            date.month = now.month
            date.date = now.date

            if (date.time < now.time) { // // 目标时间小于现在
                date.date += 1
            }
            return DataAlarm(time = date.time)


        }else if(reminderType == FlutterDetail.Reminder.Type.WEEK){ //每周 （0000000 14:41:22）

            val values = reminderValue?.split(FlutterDetail.Reminder.SPLIT)
            val week = values?.get(0)?.toByte(2)?.toInt()
            if(week != null && week > 0){

                val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)
                val date = simpleDateFormat.parse(values.get(1))
                val now = Date()

                date.year = now.year
                date.month = now.month
                date.date = now.date

                if (date.time < now.time) { //当前时间
                    date.date += 1
                }

                val calendar = Calendar.getInstance()
                calendar.timeInMillis = date.time

                for (i in 0..6){

                    val dayOfWeek = WeekUtils.getDayOfWeek(calendar)
                    if(WeekUtils.isContains(week, dayOfWeek)){

                        return DataAlarm(time = calendar.timeInMillis)
                    }
                    calendar.add(Calendar.DAY_OF_YEAR, 1)
                }
            }

        }else if(reminderType == FlutterDetail.Reminder.Type.BIRTH_DAY){ //生日  （07-18）
            // 提前一个月
            // 提前一周
            // 提前一天
            // 当天

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)
            val date = simpleDateFormat.parse(reminderValue) //

            val now = Date()

            date.year = now.year
            date.hours = 8
            date.minutes = 0
            date.seconds = 0

            while (true) {

                // 提前一个月
                date.month = date.month - 1
                if (date.time > now.time) {
                    return DataAlarm(time = date.time, info = "下个月过生日" )
                }

                // 提前一周
                date.month = date.month + 1
                date.date = date.date - 7
                if (date.time > now.time) {
                    return DataAlarm(time = date.time, info = "下周过生日")
                }

                // 提前一天
                date.date = date.date + 6
                if (date.time > now.time) {
                    return DataAlarm(time = date.time, info = "明天过生日")
                }

                // 当天
                date.date = date.date + 1
                if (date.time > now.time) {
                    return DataAlarm(time = date.time, info = "今天过生日")
                }
                date.year = date.year + 1 // 加一年
            }


        }else if(reminderType == FlutterDetail.Reminder.Type.HOLIDAYS){ //节假日 14:41:22）

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)

            val date = simpleDateFormat.parse(reminderValue)

            val now = Date()

            date.year = now.year
            date.month = now.month
            date.date = now.date

            if (date.time < now.time) { // // 目标时间小于现在
                date.date += 1
            }

            while (true){
                if(HolidaysUtils.isHolidays(date)) {
                    return DataAlarm(time = date.time)
                }
                date.date += 1
            }

        }else if(reminderType == FlutterDetail.Reminder.Type.NON_HOLIDAYS) { //非节假日

            val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)
            val date = simpleDateFormat.parse(reminderValue)

            val now = Date()

            date.year = now.year
            date.month = now.month
            date.date = now.date

            if (date.time < now.time) { // // 目标时间小于现在
                date.date += 1
            }
            while (true){
                if(!HolidaysUtils.isHolidays(date)){
                    return DataAlarm(time = date.time)
                }
                date.date += 1
            }

        }else if(reminderType == FlutterDetail.Reminder.Type.CYCLE) { //循环  （6000 2022-07-18 14:41:22）

            val values = reminderValue?.split(FlutterDetail.Reminder.SPLIT)
            val cycle = values?.get(0)?.toInt()

            if(cycle != null && cycle > 0){

                val reminderValue = values[1]
                val simpleDateFormat = SimpleDateFormat(reminderType.pattern, Locale.CHINA)

                val date = simpleDateFormat.parse(reminderValue)
                val now = System.currentTimeMillis()

                if(date.time < now){

                    val number : Int = ((now -  date.time) / (60_000 * cycle)).toInt() //中间间隔的时间
                    date.minutes += cycle * number // 如果中间过多就直接跳过

                    while(true){
                        if(date.time > now){ // 目标时间大于现在
                            return DataAlarm(time = date.time)
                        }
                        date.minutes += cycle
                    }
                }else{// 闹钟时间大于当前时间

                    return DataAlarm(time = date.time)
                }


            }
        }
        return null
    }





//    private fun startTimer(flutter: Flutter){
//
//        val reminder = flutter.reminder
//    }
//
//    private fun startTimer(id : Long, reminder: Reminder){
//
//        if(reminder.reminderType == Reminder.Type.FIXED){ //固定
//
//            val simpleDateFormat = SimpleDateFormat(Reminder.FORMAT_FIXED, Locale.CHINA)
//            val date = simpleDateFormat.parse(reminder.reminderValue)
//            val time = date.time
//            val now = System.currentTimeMillis()
//
//            if(time > now){ // 目标时间大于现在
//                flutterAlarm.startTimer(id.toInt(), time)
//            }
//
//
//        }else if(reminder.reminderType == Reminder.Type.YEAR){ //每年
//
//
//        }else if(reminder.reminderType == Reminder.Type.MONTH){ //每月
//
//
//        }else if(reminder.reminderType == Reminder.Type.DAY){ //每天
//
//        }else if(reminder.reminderType == Reminder.Type.WEEK){ //每周
//
//        }else if(reminder.reminderType == Reminder.Type.BIRTH_DAY){ //生日
//
//        }else if(reminder.reminderType == Reminder.Type.HOLIDAYS){ //节假日
//
//        }else if(reminder.reminderType == Reminder.Type.NON_HOLIDAYS) { //非节假日
//
//        }else if(reminder.reminderType == Reminder.Type.CYCLE) { //循环假日
//
//
//        }
//
//    }


}