package com.hiva.flutter.server.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.hiva.flutter.server.db.entry.DataAlarm
import com.hiva.flutter.server.db.entry.DataFlutter
import com.hiva.flutter.server.db.entry.DataSchedule

/**
 *
 * Create By HuangXiangXiang 2022/4/13
 */
@Database(entities = [DataFlutter::class, DataSchedule::class, DataAlarm::class], version = 1)
abstract class FlutterDB: RoomDatabase()  {

    abstract fun flutterDao(): FlutterDao

    companion object {

        private const val DATA_BASE_NAME = "flutter.db" // 数据库名称
        @Volatile
        private var INSTANCE: FlutterDB? = null

        fun getInstance(context: Context): FlutterDB =
            INSTANCE?: synchronized(this){
                INSTANCE?:buildDatabase(context).also {
                    INSTANCE = it
                }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, FlutterDB::class.java, DATA_BASE_NAME)
//                .allowMainThreadQueries() // 运行在主线程操作（容易造成ANR）
                .build()
    }


}