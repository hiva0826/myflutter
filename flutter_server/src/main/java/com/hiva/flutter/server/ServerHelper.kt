package com.hiva.flutter.server

import android.content.Context
import com.hiva.aidl.server.RequestBinderImpl

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
object ServerHelper {

    /**
     * 后台数据从这里开始启动
     * */
    fun startUp(context: Context){

        /**设置一下常量*/
        RequestBinderImpl.communication = ServerCommunication(context)
    }


}