package com.hiva


/**
 *
 * Create By HuangXiangXiang 2022/7/14
 *
 * 通讯
 */
interface ICommunication {

    /**
     * 请求数据
     * @param from 来源
     * @param type 类似 网络的 url
     * @param message 请求参数
     * @param listener 回调函数
     */
    fun request(from: String, type: String, message: String?, listener: OnResponseListener?)


    /**
     * 响应数据
     * */
    interface OnResponseListener{

        fun onResponse(message: String)
    }




}