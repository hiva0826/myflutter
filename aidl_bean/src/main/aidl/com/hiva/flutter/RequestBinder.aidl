package com.hiva.flutter;

import com.hiva.flutter.OnResponseListener ;

interface RequestBinder {

    /**
    * 请求数据
    * from 标识 来源
    * type 类型
    * message 参数数据
    * listener 回调
    */
    void reueset(String from, String type, String message, OnResponseListener listener) ;

}