package com.hiva.aidl.server

import android.app.Service
import android.content.Intent
import android.os.IBinder

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
class AidlService: Service() {

    override fun onBind(intent: Intent?): IBinder {
        return RequestBinderImpl()
    }

}