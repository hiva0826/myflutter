package com.hiva.aidl.server

import com.hiva.ICommunication
import com.hiva.flutter.OnResponseListener
import com.hiva.flutter.RequestBinder
import com.hiva.helper.json.JsonUtils
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
class RequestBinderImpl: RequestBinder.Stub() {


    companion object{
        var communication: ICommunication? = null
    }

    override fun reueset(
        from: String,
        type: String,
        json: String?,
        listener: OnResponseListener?
    ) {
        communication?.request(from, type, json, transformListener(listener))
    }


    private fun transformListener(listener: OnResponseListener?): ICommunication.OnResponseListener? {

        if (listener == null) return null
        return object : ICommunication.OnResponseListener {

            override fun onResponse(message: String) {
                listener.onResponse(message)
            }
        }
    }



}