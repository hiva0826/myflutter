package com.hiva.aidl.client

import android.content.Context
import com.hiva.ICommunication
import com.hiva.aidl.bean.Constant
import com.hiva.flutter.OnResponseListener
import com.hiva.flutter.RequestBinder
import com.hiva.helper.app.AidlHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
class ClientCommunication(context: Context, packageName:String) : ICommunication {

    private val aidlHelper =
        AidlHelper(context, packageName, Constant.ACTION_COMMUNICATION, RequestBinder::class.java)

    override fun request(
        from: String,
        type: String,
        message: String?,
        listener: ICommunication.OnResponseListener?
    ) {

        aidlHelper.executeTask { service ->
            service.reueset(
                from,
                type,
                message,
                transformListener(listener)
            )
        }

    }

    private fun transformListener(listener: ICommunication.OnResponseListener?): OnResponseListener? {

        if (listener == null) return null
        return object : OnResponseListener.Stub() {
            override fun onResponse(message: String) {
                listener.onResponse(message)
            }
        }
    }




}