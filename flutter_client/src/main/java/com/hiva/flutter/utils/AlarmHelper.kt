package com.hiva.flutter.utils

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.hiva.flutter.bean.response.ResponseChangedAlarm
import com.hiva.flutter.bean.response.ResponseUnReadAlarmSize
import com.hiva.flutter.function.ClientFlutterFunction
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.flutter.mvvm.view.activity.AlarmActivity
import com.hiva.helper.log.LogHelper

/**
 *
 * Create By HuangXiangXiang 2022/8/10
 * 闹钟数据
 */
object AlarmHelper {


    /**
     * 全部没有读取的数量
     * */
    val unReadAlarmSize = MutableLiveData<Int>()

    /**开始监听*/
    fun startAlarm(app: Application){

        val function = ClientFlutterFunction.getInstance(app)

        queryUnReadAlarmSize(function)
        function.setAlarmChangedListener(object: OnResponseListener<ResponseChangedAlarm> {
            override fun onSuccess(t: ResponseChangedAlarm) {
                LogHelper.i(t) // 添加
                AlarmActivity.startActivity(app, t.alarm)

                queryUnReadAlarmSize(function)
            }
            override fun onFail(code: Int, message: String?) {
                LogHelper.i("$code : $message")
            }
        })

    }


    /**
     * 查询未读的警报
     * */
    private fun queryUnReadAlarmSize(function: ClientFlutterFunction){

        function.queryUnReadAlarmSize(object: OnResponseListener<ResponseUnReadAlarmSize> {
            override fun onSuccess(t: ResponseUnReadAlarmSize) {
                unReadAlarmSize.postValue(t.size)
            }
            override fun onFail(code: Int, message: String?) {
            }
        })
    }





}


