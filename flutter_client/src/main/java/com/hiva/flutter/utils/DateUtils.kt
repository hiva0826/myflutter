package com.hiva.flutter.utils

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/8/4
 *
 *
 */
object DateUtils {

    /**显示样式*/
    private const val PATTERN = "yyyy-MM-dd HH:mm"
    private val FORMAT = SimpleDateFormat(PATTERN, Locale.CHINA)
    private val DATE = Date()

    fun toDateString(time: Long?): String {

        DATE.time = time?:System.currentTimeMillis()
        return FORMAT.format(DATE)
    }

    fun toTimeLong(date: String?): Long? {

        if(date == null) return null

        return try {
            FORMAT.parse(date).time
        }catch (e: Exception){
            null
        }
    }


}