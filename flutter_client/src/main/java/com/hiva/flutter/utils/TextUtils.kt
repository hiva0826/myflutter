package com.hiva.flutter.utils

import com.hiva.flutter.bean.Alarm
import com.hiva.flutter.bean.Filter
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.FlutterDetail

/**
 *
 * Create By HuangXiangXiang 2022/8/4
 *
 * 显示相关的类型
 *
 *
 */
object TextUtils {



    /**
     * */
    fun toEndTimeString(flutter: Flutter): String {
        return DateUtils.toDateString(flutter.endTime)
    }

    /**
     * */
    fun toEndTimeString(flutter: FlutterDetail): String {
        return DateUtils.toDateString(flutter.endTime)
    }


    /**
     * */
    fun toTimeString(alarm: Alarm): String {
        return DateUtils.toDateString(alarm.time)
    }



    fun toShowValue(filter: Filter) : String{

        val type = filter.name.type
        if(type == "createTime" || type == "endTime") {

            val time: Long? =
                try {
                    filter.value.toLong()
                }catch (e : Exception){
                    null
                }
            return DateUtils.toDateString(time)

        }else if(type == "level"){

            return try {
                filter.value.toInt()
                filter.value
            }catch (e : Exception){
                1.toString()
            }

        }else if(type == "state"){

            return try {
                FlutterDetail.State.valueOf(filter.value)
                filter.value
            }catch (e : Exception){
                FlutterDetail.State.PLAN.toString()
            }

        }else {

            return filter.value
        }

    }


}