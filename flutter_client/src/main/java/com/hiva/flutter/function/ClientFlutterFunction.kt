package com.hiva.flutter.function

import android.content.Context
import com.hiva.ICommunication
import com.hiva.aidl.client.ClientCommunication
import com.hiva.flutter.CommunicationConstant
import com.hiva.flutter.FlutterFunction
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.response.*
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.helper.json.JsonUtils

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 *
 * 客户端功能
 */
class ClientFlutterFunction private constructor(context: Context) : FlutterFunction {

    companion object{

        @Volatile
        private var INSTANCE: ClientFlutterFunction? = null

        fun getInstance(context: Context): ClientFlutterFunction =
            INSTANCE?: synchronized(this){
                INSTANCE?: ClientFlutterFunction(context.applicationContext) .also { INSTANCE = it }
            }
    }

    private var from :String = context.packageName
    private val communication : ICommunication = ClientCommunication(context, from)
    override fun setAliveFlutterChangedListener(listener: OnResponseListener<ResponseChangedAliveFlutter>?) {
        request(CommunicationConstant.TOPIC_ALIVE_FLUTTERS,null, listener)
    }


    override fun queryAliveFlutters(listener: OnResponseListener<ResponseAliveFlutters>) {
        request(CommunicationConstant.REQUEST_QUERY_ALIVE_FLUTTERS,null, listener)
    }

    override fun querySortNames(listener: OnResponseListener<ResponseSortNames>) {
        request(CommunicationConstant.REQUEST_QUERY_SORT_NAMES,null, listener)
    }

    override fun queryFilterNames(listener: OnResponseListener<ResponseFilterNames>) {
        request(CommunicationConstant.REQUEST_QUERY_FILTER_NAMES,null, listener)
    }

    override fun queryFlutters(requestFilters: RequestFilters, listener: OnResponseListener<ResponseFlutters>) {
        request(CommunicationConstant.REQUEST_QUERY_FLUTTERS,requestFilters, listener)
    }

    override fun insertOrUpdateFlutter(
        flutterDetail: FlutterDetail,
        listener: OnResponseListener<ResponseBoolean>
    ) {
        request(CommunicationConstant.REQUEST_INSERT_OR_UPDATE_FLUTTER, flutterDetail, listener)
    }

    override fun deleteFlutter(id: Long, listener: OnResponseListener<ResponseBoolean>) {
        request(CommunicationConstant.REQUEST_DELETE_FLUTTER,id, listener)
    }

    override fun queryDetailFlutter(id: Long, listener: OnResponseListener<ResponseFlutterDetail>) {
        request(CommunicationConstant.REQUEST_QUERY_DETAIL_FLUTTER,id, listener)
    }

    override fun setAlarmChangedListener(listener: OnResponseListener<ResponseChangedAlarm>?) {
        request(CommunicationConstant.TOPIC_ALARM,null, listener)
    }

    override fun queryUnReadAlarm(listener: OnResponseListener<ResponseUnReadAlarm>) {
        request(CommunicationConstant.REQUEST_QUERY_UN_READ_ALARM, null, listener)
    }

    override fun queryUnReadAlarmSize(listener: OnResponseListener<ResponseUnReadAlarmSize>) {
        request(CommunicationConstant.REQUEST_QUERY_UN_READ_ALARM_SIZE, null, listener)
    }

    override fun readAlarm(id: Long, listener: OnResponseListener<ResponseBoolean>) {
        request(CommunicationConstant.REQUEST_READ_ALARM, id, listener)
    }

    override fun readAllAlarm(listener: OnResponseListener<ResponseBoolean>) {
        request(CommunicationConstant.REQUEST_READ_ALL_ALARM,null, listener)
    }

    /**
     * 请求数据
     * */
    private fun <T> request(type: String, parameter: Any?, listener: OnResponseListener<T>?){
        communication.request(from, type, JsonUtils.toJson(parameter), transform(listener))
    }

    /**
     * 转换
     * */
    private fun <T> transform(listener: OnResponseListener<T>?): ICommunication.OnResponseListener?{

        if(listener == null) return null
        return object : ICommunication.OnResponseListener{
            override fun onResponse(message: String) {

                val response: Response<*>? = JsonUtils.parseJson(message, Response::class.java)
                if (response == null) {
                    listener.onFail(Response.CODE_INNER, "response 数据为空")
                    return
                }
                val tClass = listener.getTClass()
                val dataJson = JsonUtils.toJson(response.data)
                val t: T? = JsonUtils.parseJson(dataJson, tClass)

                if (t != null) { // 有数据
                    listener.onSuccess(t)
                } else {
                    if (response.isSuccess()) {
                        listener.onFail(Response.CODE_INNER, "data 数据为空")
                    } else {
                        listener.onFail(response.code, response.message)
                    }
                }
            }
        }

    }

}