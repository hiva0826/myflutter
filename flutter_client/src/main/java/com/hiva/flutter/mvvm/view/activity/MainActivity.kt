//package com.hiva.flutter.mvvm.view.activity
//
//import android.content.DialogInterface
//import android.os.Bundle
//import android.text.TextUtils
//import android.view.LayoutInflater
//import android.view.View
//import android.widget.AdapterView
//import android.widget.EditText
//import android.widget.Toast
//import androidx.appcompat.app.AlertDialog
//import androidx.appcompat.app.AppCompatActivity
//import androidx.core.view.GravityCompat
//import androidx.lifecycle.ViewModelProvider
//import androidx.recyclerview.widget.LinearLayoutManager
//import com.hiva.flutter.R
//import com.hiva.flutter.databinding.ActivityMainBinding
//import com.hiva.flutter.bean.Filter
//import com.hiva.flutter.bean.Flutter
//import com.hiva.flutter.bean.ParameterName
//import com.hiva.flutter.bean.Sort
//import com.hiva.flutter.mvvm.view.adapter.FilterAdapter
//import com.hiva.flutter.mvvm.view.adapter.FlutterAdapter
//import com.hiva.flutter.mvvm.view.adapter.SortAdapter
//import com.hiva.flutter.mvvm.view.adapter.StringAdapter
//import com.hiva.flutter.mvvm.view.dialog.EditFlutterDialog
//import com.hiva.flutter.mvvm.viewmodel.FilterValue
//import com.hiva.flutter.mvvm.viewmodel.MainViewModel
//
///***
// *  View: 对应于Activity和XML，负责View的绘制以及与用户交互。
// *
// * */
//class MainActivity : AppCompatActivity(), View.OnClickListener {
//
//    private lateinit var binding: ActivityMainBinding
//    private lateinit var mainVM: MainViewModel
//
//    private lateinit var filterValue: FilterValue
//
//
//    private lateinit var filterNames: Array<ParameterName>
//    private lateinit var filterTypes: Array<Filter.Type>
//    private lateinit var filters: MutableList<Filter>
//
//    private lateinit var sortAdapter: SortAdapter
//    private lateinit var flutterAdapter: FlutterAdapter
//
//    private lateinit var sortNameAdapter: StringAdapter
//    private lateinit var sortTypeAdapter: StringAdapter
//
//    private lateinit var filterAdapter: FilterAdapter
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        initUI()
//        initBinding()
//        initData()
//    }
//
//
//    private fun initUI(){
//
//        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
//        setContentView(binding.root)
//
////        binding.lifecycleOwner = this
//        mainVM = ViewModelProvider(this).get(MainViewModel::class.java) // 使用生命周期一样缓存
////        mainVM = MainViewModel() // 不能直接new 因为他的生命和应用一样长 数据就能
////        binding.mainViewModel = mainVM
//
//        filterValue = FilterValue(this)
//        mainVM.filterValue = filterValue
//
//        filterNames = filterValue.parameterNames
//        filterTypes = filterValue.filterTypes
//        filters = filterValue.getFilters()
//
//
//        sortAdapter = SortAdapter(this, filterValue.parameterNames, filterValue.getSort())
//        sortAdapter.onSelectListener  = object: SortAdapter.OnSelectListener{
//
//            override fun onSelect(sort: Sort) {
//
//                mainVM.queryFlutters()
//            }
//        }
//
//        binding.sortRvw.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//        binding.sortRvw.adapter = sortAdapter
//
//        flutterAdapter = FlutterAdapter(this)
//        flutterAdapter.onSelectListener = object: FlutterAdapter.OnSelectListener{
//            override fun onSelect(flutter: Flutter) {
//                showEditFlutterDialog(flutter)
//            }
//        }
//
//        binding.dataRvw.layoutManager = LinearLayoutManager(this)
//        binding.dataRvw.adapter = flutterAdapter
//
//        sortNameAdapter = StringAdapter(this, filterNames)
//        binding.filterNames.adapter = sortNameAdapter
//
//
//        sortTypeAdapter = StringAdapter(this,filterTypes)
//        binding.filterTypes.adapter = sortTypeAdapter
//        binding.filterTypes.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                val type: Filter.Type  = sortTypeAdapter.getItem(position) as Filter.Type
//
//                if(type == Filter.Type.SECTION){
//
//                    binding.filterValueSecond.visibility = View.VISIBLE
//
//                }else if(type == Filter.Type.KEYWORD){
//
//                    binding.filterValueSecond.visibility = View.INVISIBLE
//
//                }else if(type == Filter.Type.EQUAL){
//
//                    binding.filterValueSecond.visibility = View.INVISIBLE
//                }
//
//
//            }
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//            }
//        }
//
//        binding.addFilter.setOnClickListener(this)
//
//        filterAdapter = FilterAdapter(this, filterValue.getFilters())
//        filterAdapter.onSelectListener = object: FilterAdapter.OnSelectListener{
//            override fun onSelect(filter: Filter) {
//                // 选择 过滤数据 修改
//
//                binding.filterNames.setSelection(filterNames.index(filter.name))
//                binding.filterTypes.setSelection(filterTypes.index(filter.type))
//
//                val list = filter.values
//                if(list.size == 1){
//                    binding.filterValueFirst.setText(list[0])
//
//                }else if(list.size == 2){
//
//                    binding.filterValueFirst.setText(list[0])
//                    binding.filterValueSecond.setText(list[1])
//                }
//
//            }
//        }
//        binding.filters.layoutManager = LinearLayoutManager(this)
//        binding.filters.adapter = filterAdapter
//
//        binding.confirm.setOnClickListener(this)
//        binding.reset.setOnClickListener(this)
//
//    }
//
//
//    private fun <T> Array<T>.index(t: T):Int{
//        for (i in 0 until size){
//            if(get(i) == t){
//                return i
//            }
//        }
//        return -1
//    }
//
//
//    private fun initBinding(){
//
//        binding.mottoTvw.setOnClickListener(this)
//        binding.filterBtn.setOnClickListener(this)
//        binding.addBtn.setOnClickListener(this)
//
//        mainVM.motto.observe(this) {
//
//            if(!TextUtils.isEmpty(it)){
//                binding.mottoTvw.text = it
//
//            }else{
//                binding.mottoTvw.text = getString(R.string.motto_default)
//            }
//        }
//
//        mainVM.flutters.observe(this){
//
//            flutterAdapter.list = it.flutters
//        }
//
//
//    }
//
//
//    private fun initData(){
//
//        mainVM.queryMotto()
//
//        mainVM.queryFlutters()
//
//    }
//
//    override fun onClick(v: View?) {
//
//        if(v === binding.mottoTvw) {
//            showEditMotto()
//
//        }else if(v === binding.filterBtn){
//
//
//            binding.content.openDrawer(GravityCompat.END)
//
//        }else if(v === binding.addBtn){
//            showEditFlutterDialog(null)
//
//        }else if(v === binding.addFilter){ //添加过滤
//
//            val first = binding.filterValueFirst.text.toString()
//            if (first.isNullOrEmpty()){
//
//                Toast.makeText(this, "参数不能为空", Toast.LENGTH_SHORT).show()
//                return
//            }
//            val values = ArrayList<String>()
//            values.add(first)
//
//            if(binding.filterValueSecond.visibility == View.VISIBLE){
//
//                val second = binding.filterValueSecond.text.toString()
//                if (second.isNullOrEmpty()){
//
//                    Toast.makeText(this, "参数不能为空", Toast.LENGTH_SHORT).show()
//                    return
//                }
//                values.add(second)
//            }
//
//            val name = filterNames[binding.filterNames.selectedItemPosition]       // 名称
//            val type = filterTypes[binding.filterTypes.selectedItemPosition]        // 类型
//
//            val filter = Filter(name, type, values)
//
//            filterAdapter.addFilter(filter)
//
//        }else if(v === binding.reset){ //添加过滤
//
//
//            filterValue.reset()
//            filterAdapter.notifyDataSetChanged()
//            mainVM.queryFlutters()
//
//
//        }else if(v === binding.confirm){ //添加过滤
//
//            filterValue.confirm()
//            filterAdapter.notifyDataSetChanged()
//            mainVM.queryFlutters()
//            binding.content.closeDrawer(GravityCompat.END)
//        }
//
//    }
//
//    private lateinit var editMotto: EditText
//    private var editMottoDialog: AlertDialog? = null
//
//    private fun showEditMotto(){
//
//        if(editMottoDialog == null){
//            editMotto = EditText(this)
//
//            val clickListener = DialogInterface.OnClickListener { _, _ ->
//                val text = editMotto.text.toString()
//                mainVM.updateMotto(text)
//            }
//
//            editMottoDialog = AlertDialog.Builder(this)
//                .setTitle("修改")
//                .setView(editMotto)
//                .setNegativeButton("取消", null)
//                .setPositiveButton("修改", clickListener)
//                .create()
//        }
//
//
//        editMotto.setText(binding.mottoTvw.text)
//        editMotto.setSelection(editMotto.text.length)
//        editMottoDialog?.show()
//    }
//
//
//    private var editFlutterDialog: EditFlutterDialog? = null
//    private fun showEditFlutterDialog(flutter: Flutter?){
//
//
//        if(editFlutterDialog == null){
//            editFlutterDialog = EditFlutterDialog(this)
//            editFlutterDialog?.onSaveFlutterListener = object : EditFlutterDialog.OnSaveFlutterListener  {
//                override fun onSave(flutter: Flutter) {
//                    mainVM.insertOrUpdateFlutter(flutter)
//                }
//            }
//        }
//
//        editFlutterDialog?.flutter = flutter
//        editFlutterDialog?.show()
//
//    }
//
//
//}