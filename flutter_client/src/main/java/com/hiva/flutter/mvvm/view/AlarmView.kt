package com.hiva.flutter.mvvm.view

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.hiva.flutter.bean.Alarm
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.response.ResponseFlutterDetail
import com.hiva.flutter.databinding.ActivityAlarmBinding
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.flutter.mvvm.view.adapter.AlarmAdapter
import com.hiva.flutter.mvvm.view.dialog.AlarmDialog
import com.hiva.flutter.mvvm.view.dialog.EditFlutterDialog
import com.hiva.flutter.mvvm.viewmodel.AlarmViewModel
import com.hiva.helper.app.MainThreadUtils

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 *
 * view 脱离 activity 层
 *
 */
class AlarmView (activity: AppCompatActivity) : BaseView<ActivityAlarmBinding, AlarmViewModel>(activity){

    var alarm: Alarm? = null

    override fun onCreate() {
        super.onCreate()

        val adapter = AlarmAdapter(activity)
        adapter.onSelectListener = object : AlarmAdapter.OnSelectListener{
            override fun onSelect(alarm: Alarm) {
                showDialog(alarm)
            }
        }

        binding.alarmRvw.layoutManager = LinearLayoutManager(activity)
        binding.alarmRvw.adapter = adapter

        viewModel.unReadAlarms.observe(activity){

            if(it.isEmpty()){
                binding.alarmRvw.visibility = View.GONE
                binding.tipsTvw.visibility = View.VISIBLE

                binding.clearBtn.isEnabled = false

            }else{
                binding.alarmRvw.visibility = View.VISIBLE
                binding.tipsTvw.visibility = View.GONE

                adapter.list = it

                binding.clearBtn.isEnabled = true
            }
        }
        viewModel.queryUnReadAlarm()
    }


    override fun onStartData() {
        super.onStartData()

        alarm?.let { showDialog(it) }
    }


    fun readAll(view: View){
        viewModel.readAllAlarm()
    }

    private var dialog: AlarmDialog? = null
    private fun showDialog(alarm: Alarm){

        if(dialog == null){
            dialog = AlarmDialog(activity)
        }
        dialog!!.show(alarm, object : AlarmDialog.OnClickListener{
            override fun read() {

                viewModel.readAlarm(alarm.id)
            }

            override fun detail() {

                viewModel.queryDetailFlutter(alarm.fId, object :
                    OnResponseListener<ResponseFlutterDetail> {
                    override fun onSuccess(t: ResponseFlutterDetail) {

                        MainThreadUtils.executeInMain{
                            show(t.flutterDetail)
                        }
                    }

                    override fun onFail(code: Int, message: String?) {
                    }
                })

            }
        })

    }

    private var editFlutterDialog: EditFlutterDialog? = null
    private fun show(flutter: FlutterDetail?){

        if(editFlutterDialog == null){
            editFlutterDialog = EditFlutterDialog(activity)
            editFlutterDialog!!.onSaveFlutterListener = object : EditFlutterDialog.OnSaveFlutterListener{
                override fun onSave(flutter: FlutterDetail) {
                    // 保存（数据）
                    viewModel.insertOrUpdateFlutter(flutter)
                }
            }
        }

        editFlutterDialog?.let {
            it.show(flutter)
        }
    }




}