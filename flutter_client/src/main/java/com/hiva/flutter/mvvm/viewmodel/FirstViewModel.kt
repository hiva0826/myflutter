package com.hiva.flutter.mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.response.ResponseAliveFlutters
import com.hiva.flutter.bean.response.ResponseBoolean
import com.hiva.flutter.bean.response.ResponseChangedAliveFlutter
import com.hiva.flutter.bean.response.ResponseFlutterDetail
import com.hiva.flutter.listener.OnResponseListener
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 */
class FirstViewModel: BaseViewModel() {

    val aliveFlutters = MutableLiveData<List<Flutter>>()

    override fun onCleared() {
        super.onCleared()
        aliveFlutters.value = null
    }

    /**查询相信的数据*/
    fun startAliveFlutterChangedListener(){

        function.setAliveFlutterChangedListener(object: OnResponseListener<ResponseChangedAliveFlutter>{
            override fun onSuccess(t: ResponseChangedAliveFlutter) {
                queryAliveFlutters()
            }
            override fun onFail(code: Int, message: String?) {
            }

        })
    }

    fun stopAliveFlutterChangedListener(){

        function.setAliveFlutterChangedListener(null)
    }


    /**查询活跃的数据*/
    fun queryAliveFlutters(){

        function.queryAliveFlutters(object: OnResponseListener<ResponseAliveFlutters>{
            override fun onSuccess(t: ResponseAliveFlutters) {

                val flutters: List<Flutter> = t.flutters

//                Collections.sort(flutters ) { f1, f2 -> f1.endTime.compareTo(f2.endTime) }
                flutters.sortedByDescending { it.endTime }

                aliveFlutters.postValue(flutters)
            }
            override fun onFail(code: Int, message: String?) {
            }
        })
    }

    /**修改*/
    fun insertOrUpdateFlutter(flutter: FlutterDetail) {

        function.insertOrUpdateFlutter(flutter, object: OnResponseListener<ResponseBoolean>{
            override fun onSuccess(t: ResponseBoolean) {
                queryAliveFlutters()
            }

            override fun onFail(code: Int, message: String?) {
            }
        })
    }


    /**查询相信的数据*/
    fun queryDetailFlutter(id: Long, listener: OnResponseListener<ResponseFlutterDetail>){

        function.queryDetailFlutter(id , listener)
    }




}