package com.hiva.flutter.mvvm

import android.app.Application
import com.hiva.flutter.utils.AlarmHelper
import com.hiva.flutter.server.ServerHelper


/**
 *
 * Create By HuangXiangXiang 2022/5/18
 */
class FlutterApplication: Application() {

    companion object{

        lateinit var context: Application
    }


    override fun onCreate() {
        super.onCreate()
        context = this

        ServerHelper.startUp(this)

        AlarmHelper.startAlarm(this)


    }


}