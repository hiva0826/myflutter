package com.hiva.flutter.mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.Name
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.response.*
import com.hiva.flutter.listener.OnResponseListener

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 */
class QueryViewModel: BaseViewModel() {

    /**过滤条件*/
    val requestFilters = RequestFilters()

    val sorts = MutableLiveData<List<Name>>()
    val filters = MutableLiveData<List<Name>>()
    val flutters = MutableLiveData<List<FlutterDetail>>()


    fun querySorts(){

        function.querySortNames(object: OnResponseListener<ResponseSortNames>{
            override fun onSuccess(t: ResponseSortNames) {
                sorts.postValue(t.sorts)
            }
            override fun onFail(code: Int, message: String?) {
            }
        })
    }

    fun queryFilters(){

        function.queryFilterNames(object : OnResponseListener<ResponseFilterNames>{
            override fun onSuccess(t: ResponseFilterNames) {
                filters.postValue(t.filters)
            }
            override fun onFail(code: Int, message: String?) {
            }
        })
    }



    fun queryFlutters(){

        function.queryFlutters(requestFilters, object : OnResponseListener<ResponseFlutters>{
            override fun onSuccess(t: ResponseFlutters) {
                flutters.postValue(t.flutters)
            }
            override fun onFail(code: Int, message: String?) {
            }
        })
    }

    fun insertOrUpdateFlutter(flutter: FlutterDetail ) {
        function.insertOrUpdateFlutter(flutter, object :OnResponseListener<ResponseBoolean>{
            override fun onSuccess(t: ResponseBoolean) {
                if(t.result){
                    queryFlutters()
                }
            }

            override fun onFail(code: Int, message: String?) {
            }
        })
    }


}