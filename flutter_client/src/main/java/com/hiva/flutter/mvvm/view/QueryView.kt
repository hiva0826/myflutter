package com.hiva.flutter.mvvm.view

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.hiva.flutter.bean.Filter
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.Name
import com.hiva.flutter.databinding.ActivityQueryBinding
import com.hiva.flutter.mvvm.view.adapter.FilterAdapter
import com.hiva.flutter.mvvm.view.adapter.FlutterDetailAdapter
import com.hiva.flutter.mvvm.view.adapter.SortAdapter
import com.hiva.flutter.mvvm.view.adapter.StringAdapter
import com.hiva.flutter.mvvm.view.dialog.*
import com.hiva.flutter.mvvm.viewmodel.QueryViewModel
import com.hiva.flutter.utils.DateUtils

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 *
 * view 脱离 activity 层
 *
 */
class QueryView (activity: AppCompatActivity) : BaseView<ActivityQueryBinding, QueryViewModel>(activity){

    private var editFlutterDialog: EditFlutterDialog? = null

    private lateinit var filterNameAdapter:StringAdapter
    /**过滤数据*/
    private lateinit var filterAdapter: FilterAdapter

    override fun onCreate() {
        super.onCreate()

        val sortAdapter = SortAdapter(activity, viewModel.requestFilters.sort)
        sortAdapter.onSelectListener  = object: SortAdapter.OnSelectListener{
            override fun onSelect() { // 切换
                viewModel.queryFlutters()
            }
        }

        binding.sortRvw.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.sortRvw.adapter = sortAdapter
        viewModel.sorts.observe(activity, {
            sortAdapter.list = it
        })
        viewModel.querySorts()

        val flutterAdapter = FlutterDetailAdapter(activity)
        flutterAdapter.onSelectListener = object : FlutterDetailAdapter.OnSelectListener{
            override fun onSelect(flutter: FlutterDetail) {

                if(editFlutterDialog == null){
                    editFlutterDialog = EditFlutterDialog(activity)
                    editFlutterDialog!!.onSaveFlutterListener = object : EditFlutterDialog.OnSaveFlutterListener{
                        override fun onSave(flutter: FlutterDetail) {
                            // 保存（数据）
                            viewModel.insertOrUpdateFlutter(flutter)
                        }
                    }
                }

                editFlutterDialog?.show(flutter.copy())

            }
        }
        binding.dataRvw.layoutManager = LinearLayoutManager(activity)
        binding.dataRvw.adapter = flutterAdapter
        viewModel.flutters.observe(activity, {
            flutterAdapter.list = it
        })
        viewModel.queryFlutters()

        // 过滤名称
        filterNameAdapter = StringAdapter(activity)
        viewModel.filters.observe(activity, {
            filterNameAdapter.list = it
        })
        binding.filterNameSpr.adapter = filterNameAdapter
        viewModel.queryFilters()

        // 过滤条件
        val filterTypeAdapter = StringAdapter(activity, Filter.Type.values())
        binding.filterTypeSpr.adapter = filterTypeAdapter

        filterAdapter = FilterAdapter(activity, viewModel.requestFilters.list)
        filterAdapter.onSelectListener = object : FilterAdapter.OnSelectListener{
            override fun onSelect(filter: Filter) {
                refreshFilter(filter)
            }
        }
        binding.filtersRvw.layoutManager = LinearLayoutManager(activity)
        binding.filtersRvw.adapter = filterAdapter
    }


    /**过滤*/
    fun filter(view: View){

//        refreshFilter(null)
        binding.content.openDrawer(GravityCompat.END)
    }


    fun add(view: View){

        val name = filterNameAdapter.list?.get(binding.filterNameSpr.selectedItemPosition) as Name      // 名称
        val showValue: String = binding.filterValueTvw.text.toString()       // 参数

        val value = show2Real(name, showValue)
        if(value == null){
            showInputValue(name, "")
            return
        }

        val type: Filter.Type = Filter.Type.values()[binding.filterTypeSpr.selectedItemPosition]           // 类型
        val filter = Filter(name, type, value)

        filterAdapter.addFilter(filter)
        refreshFilter(null)
    }

    /**真实数据转换成显示数据*/
    private fun real2Show(name: Name, real: String): String?{

        val type = name.type
        if(type == "createTime" || type == "endTime") {

            return try {
                val time = real.toLong()
                DateUtils.toDateString(time)
            }catch (e : Exception){

                null
            }
        }
        return real
    }

    /**显示数据转换成真实数据*/
    private fun show2Real(name: Name, show: String): String?{

        val type = name.type
        if(type == "createTime" || type == "endTime") {
            try {
                return DateUtils.toTimeLong(show)?.toString()
            }catch (e : Exception){ }

        }else if(type == "level"){

            try {
                show.toInt()
                return show
            }catch (e : Exception){ }

        }else if(type == "state"){
            try {
                FlutterDetail.State.valueOf(show)
                return show
            }catch (e : Exception){ }
        }else {

            if(show.isNotEmpty()){
                return show
            }
        }

        return null
    }

    private fun showInputValue(name: Name , value : String){

        val type = name.type
        if(type == "createTime" || type == "endTime") {

            // 时间格式
            val time : Long = try {
                value.toLong()
            }catch (e : Exception){
                System.currentTimeMillis()
            }

            val dialog = DateTimeDialog(activity)
            dialog.setOnSelectDateTimeListener(object : DateTimeDialog.OnSelectDateTimeListener{
                override fun onSelectDateTime(time: Long) {
                        // 输入
                    binding.filterValueTvw.text = DateUtils.toDateString(time)
                }
            })
            dialog.show(time)

        }else if(type == "level"){

            // 等级
            val dialog = InputIntDialog(activity)
            dialog.show(value, object : InputBaseDialog.OnConfirmListener{
                override fun onConfirm(value: String) {
                    binding.filterValueTvw.text = value
                }
            })

        }else if(type == "state"){

            // 级别
            val dialog = InputStateDialog(activity)
            dialog.show(value, object : InputBaseDialog.OnConfirmListener{
                override fun onConfirm(value: String) {
                    binding.filterValueTvw.text = value
                }
            })

        }else {

            // 普通text
            val dialog = InputTextDialog(activity)
            dialog.show(value, object : InputBaseDialog.OnConfirmListener{
                override fun onConfirm(value: String) {
                    binding.filterValueTvw.text = value
                }
            })
        }

    }



    fun inputValue(view: View){

        val name = filterNameAdapter.list?.get(binding.filterNameSpr.selectedItemPosition) as Name      // 名称
        val value: String = binding.filterValueTvw.text.toString()       // 参数
        showInputValue(name, value)
    }


    fun reset(view: View){

        binding.content.closeDrawer(GravityCompat.END)

        viewModel.requestFilters.list.clear()
        filterAdapter.notifyDataSetChanged()

        refreshFilter(null)

        viewModel.queryFlutters()
    }

    fun confirm(view: View){

        binding.content.closeDrawer(GravityCompat.END)

        viewModel.queryFlutters()
    }

    /**清空数据*/
    private fun refreshFilter(filter: Filter?){

        if(filter == null){

            binding.filterNameSpr.setSelection(0)
            binding.filterTypeSpr.setSelection(0)

            binding.filterValueTvw.text = null

        }else{

            binding.filterNameSpr.setSelection(filterNameAdapter.indexOf(filter.name))
            binding.filterTypeSpr.setSelection(filter.type.ordinal)

            binding.filterValueTvw.text = real2Show (filter.name, filter.value)
        }

    }




}