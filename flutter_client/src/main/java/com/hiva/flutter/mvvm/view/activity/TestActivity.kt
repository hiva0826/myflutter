package com.hiva.flutter.mvvm.view.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.hiva.flutter.bean.response.ResponseBoolean
import com.hiva.flutter.bean.response.ResponseChangedAlarm
import com.hiva.flutter.databinding.ActivityTestBinding
import com.hiva.flutter.function.ClientFlutterFunction
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.helper.log.LogHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
class TestActivity: AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityTestBinding

    private lateinit var function: ClientFlutterFunction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        function = ClientFlutterFunction.getInstance(this)

        binding = ActivityTestBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)


        binding.startBtn.setOnClickListener(this)
        binding.stopBtn.setOnClickListener(this)
        binding.queryBtn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {

        if(v == binding.startBtn ){

            function.setAlarmChangedListener( object : OnResponseListener<ResponseChangedAlarm>{

                override fun onSuccess(t: ResponseChangedAlarm) {
                    LogHelper.i(t)
                }
                override fun onFail(code: Int, message: String?) {
                    LogHelper.i("$code: $message")
                }

            })

        }else if(v == binding.stopBtn ){

            function.setAlarmChangedListener(null)

        }else if(v == binding.queryBtn ){

            function.deleteFlutter( index++, object : OnResponseListener<ResponseBoolean>{

                override fun onSuccess(t: ResponseBoolean) {
                    LogHelper.i(t.result)
                }

                override fun onFail(code: Int, message: String?) {

                    LogHelper.i("$code: $message")
                }
            })

        }
    }

    var index = 0L
}