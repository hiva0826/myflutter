package com.hiva.flutter.mvvm.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.hiva.flutter.bean.Alarm
import com.hiva.flutter.databinding.DialogAlarmBinding
import com.hiva.flutter.utils.DateUtils

/**
 *
 * Create By HuangXiangXiang 2022/8/11
 */
class AlarmDialog (context: Context) : Dialog(context), View.OnClickListener {


    lateinit var binding: DialogAlarmBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogAlarmBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.cancelBtn.setOnClickListener(this)
        binding.readBtn.setOnClickListener(this)
        binding.detailBtn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {

        if(v  == binding.cancelBtn){
            dismiss()

        }else if(v  == binding.readBtn) {
            dismiss()
            listener?.read()

        }else if(v  == binding.detailBtn) {
            listener?.detail()
        }
    }



    private var listener: OnClickListener? = null
    interface OnClickListener{

        fun read()
        fun detail()
    }


    fun show(alarm: Alarm ,listener: OnClickListener? ){
        show()
        this.listener = listener

        binding.titleTvw.text = alarm.title
        binding.timeTvw.text = DateUtils.toDateString(alarm.time)
        binding.infoTvw.text = alarm.info

    }




}