package com.hiva.flutter.mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import com.hiva.flutter.bean.Alarm
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.response.ResponseBoolean
import com.hiva.flutter.bean.response.ResponseFlutterDetail
import com.hiva.flutter.bean.response.ResponseUnReadAlarm
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.flutter.utils.AlarmHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 */
class AlarmViewModel: BaseViewModel() {

    val flutterAndroidViewModel = AlarmHelper

    val unReadAlarms = MutableLiveData<List<Alarm>>()

    /**
     * 查询未读的警报
     * */
    fun queryUnReadAlarm() {

        function.queryUnReadAlarm(object : OnResponseListener<ResponseUnReadAlarm> {

            override fun onSuccess(t: ResponseUnReadAlarm) {
                unReadAlarms.postValue(t.alarms)
                flutterAndroidViewModel.unReadAlarmSize.postValue(t.alarms.size)
            }

            override fun onFail(code: Int, message: String?) {
            }
        })
    }


    fun readAlarm(id: Long){

        function.readAlarm(id, object: OnResponseListener<ResponseBoolean> {

            override fun onSuccess(t: ResponseBoolean) {
                if(t.result){
                    queryUnReadAlarm()
                }
            }

            override fun onFail(code: Int, message: String?) {
            }

        })
    }


    fun readAllAlarm(){

        function.readAllAlarm(object: OnResponseListener<ResponseBoolean> {

            override fun onSuccess(t: ResponseBoolean) {
                if(t.result){
                    queryUnReadAlarm()
                }
            }

            override fun onFail(code: Int, message: String?) {
            }
        })
    }




    /**修改*/
    fun insertOrUpdateFlutter(flutter: FlutterDetail) {

        function.insertOrUpdateFlutter(flutter, object: OnResponseListener<ResponseBoolean>{
            override fun onSuccess(t: ResponseBoolean) {
            }

            override fun onFail(code: Int, message: String?) {
            }
        })
    }


    /**查询相信的数据*/
    fun queryDetailFlutter(id: Long, listener: OnResponseListener<ResponseFlutterDetail>){

        function.queryDetailFlutter(id , listener)
    }


}