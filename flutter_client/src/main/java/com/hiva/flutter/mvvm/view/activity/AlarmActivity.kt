package com.hiva.flutter.mvvm.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import com.hiva.flutter.bean.Alarm
import com.hiva.flutter.mvvm.view.AlarmView
import com.hiva.helper.json.JsonUtils

/**
 *
 * Create By HuangXiangXiang 2022/7/19
 *
 * 闹钟到
 */
class AlarmActivity: BaseActivity<AlarmView> (){

    companion object{

        private const val KEY_ALARM = "alarm"

        fun startActivity(context: Context, alarm: Alarm? = null){

            val intent = Intent(context, AlarmActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            alarm?.let {
                intent.putExtra(KEY_ALARM, JsonUtils.toJson(it))
            }
            context.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        initValue(intent)
        super.onCreate(savedInstanceState)

        view.onStartData()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            initValue(it)
            view.onStartData()
        }
    }


    private fun initValue(intent: Intent){
        val json = intent.getStringExtra(KEY_ALARM)
        view.alarm = JsonUtils.parseJson(json, Alarm::class.java)
    }

}