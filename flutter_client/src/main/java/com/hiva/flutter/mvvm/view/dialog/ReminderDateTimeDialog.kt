package com.hiva.flutter.mvvm.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.LinearLayout
import android.widget.NumberPicker
import android.widget.Toast
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.databinding.DialogReminderDateTimeBinding
import com.hiva.flutter.utils.WeekUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/5/26
 */
class ReminderDateTimeDialog(context: Context) : Dialog(context) {

    companion object{

        /**
         * 默认循环时间（单位分钟）
         * */
        private const val CYCLE_DEFAULT = 1

        /**
         * 默认重复星期
         * */
        private const val WEEK_DEFAULT = 0b1000001


        fun getSpinner(datePicker : DatePicker, spinnerName: String): NumberPicker {

            val clazz = datePicker.javaClass

            val mDelegateField = clazz.getDeclaredField("mDelegate")
            mDelegateField.isAccessible = true

            val mDelegate = mDelegateField.get(datePicker)
            val clazz1 = mDelegate.javaClass

            val yearField = clazz1.getDeclaredField(spinnerName)
            yearField.isAccessible = true

            val npr = yearField.get(mDelegate) as NumberPicker

            val layoutParams = npr.layoutParams as LinearLayout.LayoutParams
//        layoutParams.topMargin = 0
//        layoutParams.bottomMargin = 0
            layoutParams.leftMargin= 0
            layoutParams.rightMargin = 0

            return npr
        }


    }

    private fun toFixLengthBinaryString(value: Int): String {
        val fixLength = 7
        val s = Integer.toBinaryString(value)
        val sb = StringBuilder(fixLength)
        for (i in s.length until fixLength) {
            sb.append("0")
        }
        sb.append(s)
        return sb.toString()
    }

    private lateinit var binding: DialogReminderDateTimeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogReminderDateTimeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.timeTpr.setIs24HourView(true)
        binding.cancelBtn.setOnClickListener(onClickListener)
        binding.confirmBtn.setOnClickListener(onClickListener)

    }

    private var year: NumberPicker? = null
    private fun getYear(): View {

        if (year == null) {
            year = getSpinner(binding.dateDpr, "mYearSpinner")
        }
        return year!!
    }

    private var month: NumberPicker? = null
    private fun getMonth(): View {

        if (month == null) {
            month = getSpinner(binding.dateDpr, "mMonthSpinner")
        }
        return month!!
    }

    private var day: NumberPicker? = null
    private fun getDay(): View {

        if (day == null) {
            day = getSpinner(binding.dateDpr, "mDaySpinner")
        }
        return day!!
    }




    private val onClickListener =
        View.OnClickListener { v ->

            if (v === binding.confirmBtn) {

                val year = binding.dateDpr.year
                val month = binding.dateDpr.month
                val day = binding.dateDpr.dayOfMonth
                val hour = binding.timeTpr.hour
                val minute = binding.timeTpr.minute

                calendar[Calendar.YEAR] = year
                calendar[Calendar.MONTH] = month
                calendar[Calendar.DAY_OF_MONTH] = day
                calendar[Calendar.HOUR_OF_DAY] = hour
                calendar[Calendar.MINUTE] = minute
                calendar[Calendar.SECOND] = 0
                calendar[Calendar.MILLISECOND] = 0

                val value: String
                when (type) {

                    FlutterDetail.Reminder.Type.WEEK-> {  // 每周     （2 14:41:22）

                        var week = 0
                        if(binding.sunCbx.isChecked){
                            week = week or WeekUtils.FLAG_SUNDAY
                        }
                        if(binding.monCbx.isChecked){
                            week = week or WeekUtils.FLAG_MONDAY
                        }
                        if(binding.tueCbx.isChecked){
                            week = week or WeekUtils.FLAG_TUESDAY
                        }
                        if(binding.wedCbx.isChecked){
                            week = week or WeekUtils.FLAG_WEDNESDAY
                        }
                        if(binding.thuCbx.isChecked){
                            week = week or WeekUtils.FLAG_THURSDAY
                        }
                        if(binding.friCbx.isChecked){
                            week = week or WeekUtils.FLAG_FRIDAY
                        }
                        if(binding.satCbx.isChecked){
                            week = week or WeekUtils.FLAG_SATURDAY
                        }

                        if(week == 0){

                            Toast.makeText(context, "至少选择一个重复的星期" , Toast.LENGTH_SHORT).show()
                            return@OnClickListener
                        }

                        value = toFixLengthBinaryString(week) + FlutterDetail.Reminder.SPLIT + simpleDateFormat.format(calendar.time)

                    }
                    FlutterDetail.Reminder.Type.CYCLE -> { // 循环   （6000 14:41:22）

                        val cycle = try {
                             binding.cycleEtt.text.toString().toInt()
                        }catch (e : Exception){
                            0
                        }

                        if(cycle <= 0){
                            Toast.makeText(context, "重复时间必须大于0" , Toast.LENGTH_SHORT).show()
                            return@OnClickListener
                        }

                        value =  cycle.toString() + FlutterDetail.Reminder.SPLIT + simpleDateFormat.format(calendar.time)
                    }
                    else -> { //每天
                        value = simpleDateFormat.format(calendar.time)
                    }
                }

                if (this@ReminderDateTimeDialog.value != value) {
                    onSelectDateTimeListener?.onSelectDateTime(value)
                }
            }
            dismiss()
        }


    private val calendar = Calendar.getInstance()

    private lateinit var type: FlutterDetail.Reminder.Type
    private lateinit var value : String
    private val simpleDateFormat = SimpleDateFormat("", Locale.CHINA)


    fun show( type: FlutterDetail.Reminder.Type, value: String):String {
        show()

        this.type = type
        this.value = value
        simpleDateFormat.applyPattern(type.pattern)

        when (type) {
            FlutterDetail.Reminder.Type.FIXED -> { //固定

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.VISIBLE    // 年
                getMonth().visibility = View.VISIBLE   // 月
                getDay().visibility = View.VISIBLE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }
                calendar.timeInMillis = date.time

                val year = calendar[Calendar.YEAR]
                val month = calendar[Calendar.MONTH]
                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute

            }
            FlutterDetail.Reminder.Type.YEAR -> { //每年

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.VISIBLE   // 月
                getDay().visibility = View.VISIBLE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
                val year = calendar[Calendar.YEAR]
                val month = calendar[Calendar.MONTH]
                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute
            }

            FlutterDetail.Reminder.Type.MONTH -> { //每月

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.GONE   // 月
                getDay().visibility = View.VISIBLE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
                val year = calendar[Calendar.YEAR]
                val month = calendar[Calendar.MONTH]
                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute

            }
            FlutterDetail.Reminder.Type.DAY -> { //每天

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.GONE   // 月
                getDay().visibility = View.GONE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
                val year = calendar[Calendar.YEAR]
                val month = calendar[Calendar.MONTH]
                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute


            }
            FlutterDetail.Reminder.Type.WEEK -> { //每周

                binding.weekTvw.visibility = View.VISIBLE
                binding.sunCbx.visibility = View.VISIBLE
                binding.monCbx.visibility = View.VISIBLE
                binding.tueCbx.visibility = View.VISIBLE
                binding.wedCbx.visibility = View.VISIBLE
                binding.thuCbx.visibility = View.VISIBLE
                binding.friCbx.visibility = View.VISIBLE
                binding.satCbx.visibility = View.VISIBLE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.GONE   // 月
                getDay().visibility = View.GONE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间


                var week = WEEK_DEFAULT
                val date = try {

                    val values = value.split(FlutterDetail.Reminder.SPLIT)
                    week = values[0].toByte(2).toInt()

                    simpleDateFormat.parse(values[1])
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = toFixLengthBinaryString(week) + FlutterDetail.Reminder.SPLIT +simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
//                val month = calendar[Calendar.MONTH]
//                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

                binding.sunCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_SUNDAY)
                binding.monCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_MONDAY)
                binding.tueCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_TUESDAY)
                binding.wedCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_WEDNESDAY)
                binding.thuCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_THURSDAY)
                binding.friCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_FRIDAY)
                binding.satCbx.isChecked = WeekUtils.isContains(week, WeekUtils.FLAG_SATURDAY)

//                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute

            }
            FlutterDetail.Reminder.Type.BIRTH_DAY -> { //生日（07-18）

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.VISIBLE   // 月
                getDay().visibility = View.VISIBLE     // 日
                binding.timeTpr.visibility = View.GONE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
                val year = calendar[Calendar.YEAR]
                val month = calendar[Calendar.MONTH]
                val day = calendar[Calendar.DAY_OF_MONTH]
//                val hour = calendar[Calendar.HOUR_OF_DAY]
//                val minute = calendar[Calendar.MINUTE]

                binding.dateDpr.init(year, month, day, null)
//                binding.timeTpr.hour = hour
//                binding.timeTpr.minute = minute


            }
            FlutterDetail.Reminder.Type.HOLIDAYS -> { //节假日（14:41:22）

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.GONE   // 月
                getDay().visibility = View.GONE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
//                val year = calendar[Calendar.YEAR]
//                val month = calendar[Calendar.MONTH]
//                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

//                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute

            }
            FlutterDetail.Reminder.Type.NON_HOLIDAYS -> { //非节假日（14:41:22）

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.GONE
                binding.cycleEtt.visibility = View.GONE

                getYear().visibility = View.GONE    // 年
                getMonth().visibility = View.GONE   // 月
                getDay().visibility = View.GONE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                val date = try {
                    simpleDateFormat.parse(value)
                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = simpleDateFormat.format(d)
                    d
                }

                calendar.timeInMillis = date.time
//                val year = calendar[Calendar.YEAR]
//                val month = calendar[Calendar.MONTH]
//                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

//                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute

            }
            FlutterDetail.Reminder.Type.CYCLE -> { //循环（6000 14:41:22）

                binding.weekTvw.visibility = View.GONE
                binding.sunCbx.visibility = View.GONE
                binding.monCbx.visibility = View.GONE
                binding.tueCbx.visibility = View.GONE
                binding.wedCbx.visibility = View.GONE
                binding.thuCbx.visibility = View.GONE
                binding.friCbx.visibility = View.GONE
                binding.satCbx.visibility = View.GONE

                binding.cycleTvw.visibility = View.VISIBLE
                binding.cycleEtt.visibility = View.VISIBLE

                getYear().visibility = View.VISIBLE    // 年
                getMonth().visibility = View.VISIBLE   // 月
                getDay().visibility = View.VISIBLE     // 日
                binding.timeTpr.visibility = View.VISIBLE // 时间

                var cycle = CYCLE_DEFAULT
                val date = try {

                    val values = value.split(FlutterDetail.Reminder.SPLIT)
                    cycle = values[0].toInt()

                    simpleDateFormat.parse(values[1])

                }catch (e : Exception){
                    val d = Date() // 当前时间
                    this.value = cycle.toString() + FlutterDetail.Reminder.SPLIT + simpleDateFormat.format(d)
                    d
                }

                binding.cycleEtt.setText(cycle.toString())


                calendar.timeInMillis = date.time
                val year = calendar[Calendar.YEAR]
                val month = calendar[Calendar.MONTH]
                val day = calendar[Calendar.DAY_OF_MONTH]
                val hour = calendar[Calendar.HOUR_OF_DAY]
                val minute = calendar[Calendar.MINUTE]

                binding.dateDpr.init(year, month, day, null)
                binding.timeTpr.hour = hour
                binding.timeTpr.minute = minute
            }
        }

        return this.value
    }


//    private var time: Long = 0
//    fun show(time: Long) {
//        show()
//
//        this.time = time
//
//        calendar.timeInMillis = time
//        val year = calendar[Calendar.YEAR]
//        val month = calendar[Calendar.MONTH]
//        val day = calendar[Calendar.DAY_OF_MONTH]
//        val hour = calendar[Calendar.HOUR_OF_DAY]
//        val minute = calendar[Calendar.MINUTE]
//
//        mDateDpr.init(year, month, day, null)
//        mTimeTpr.hour = hour
//        mTimeTpr.minute = minute
//    }

    interface OnSelectDateTimeListener {
        fun onSelectDateTime(value: String)
    }

    private var onSelectDateTimeListener: OnSelectDateTimeListener? = null
    fun setOnSelectDateTimeListener(onSelectDateTimeListener: OnSelectDateTimeListener?) {
        this.onSelectDateTimeListener = onSelectDateTimeListener
    }


}