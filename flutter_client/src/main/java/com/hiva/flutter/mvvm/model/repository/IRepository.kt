package com.hiva.flutter.mvvm.model.repository

import com.hiva.flutter.Response
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.response.ResponseAliveFlutters
import com.hiva.flutter.bean.Setting
import com.hiva.flutter.mvvm.model.listener.OnResponseListener
import com.hiva.helper.json.JsonUtils
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 *
 * Create By HuangXiangXiang 2022/4/28
 */
interface IRepository {

    private val executors: ExecutorService
        get() = Executors.newSingleThreadExecutor()

    /**增加或者设置*/
    fun insertOrUpdateSetting(setting: Setting, onResponseListener: OnResponseListener<Boolean>?){

        executors.submit {
            val result =  insertOrUpdateSettingString(JsonUtils.toJson(setting))
            val response = Response.parseJson(result, Boolean::class.java)
            if(response != null){
                onResponseListener?.onResponse(response)
            }
        }
    }
    fun insertOrUpdateSettingString(request: String): String

    /**删除设置*/
    fun deleteSetting(key: String, onResponseListener: OnResponseListener<Boolean>?){
        executors.submit {

            val setting = Setting(key , null)
            val result = deleteSettingString(JsonUtils.toJson(setting))
            val response = Response.parseJson(result, Boolean::class.java)
            if(response != null){
                onResponseListener?.onResponse(response)
            }
        }
    }
    fun deleteSettingString(request: String): String

    /**查询设置*/
    fun querySetting(key: String, onResponseListener: OnResponseListener<Setting>?){

        executors.submit {
            val setting = Setting(key , null)
            val result = querySettingString(JsonUtils.toJson(setting))
            val response = Response.parseJson(result, Setting::class.java)
            if(response != null){
                onResponseListener?.onResponse(response)
            }
        }
    }
    fun querySettingString(request: String): String


    /**增加或者任务*/
    fun insertOrUpdateFlutter(flutter: Flutter, onResponseListener: OnResponseListener<Boolean>?){

        executors.submit {
            val result =  insertOrUpdateFlutterString(JsonUtils.toJson(flutter))
            val response = Response.parseJson(result, Boolean::class.java)
            if(response != null){
                onResponseListener?.onResponse(response)
            }
        }
    }
    fun insertOrUpdateFlutterString(request: String): String

    /**删除设置*/
    fun deleteFlutter(id: Long, onResponseListener: OnResponseListener<Boolean>?){
        executors.submit {
            val data = Flutter(id)
            val result = deleteFlutterString(JsonUtils.toJson(data))
            val response = Response.parseJson(result, Boolean::class.java)
            if(response != null){
                onResponseListener?.onResponse(response)
            }
        }
    }
    fun deleteFlutterString(request: String): String

    /**查询设置*/
    fun queryFlutter(id: Long, onResponseListener: OnResponseListener<Flutter>?){

        executors.submit {
            val data = Flutter(id)
            val result = queryFlutterString(JsonUtils.toJson(data))
            val response = Response.parseJson(result, Flutter::class.java)
            if(response != null){
                onResponseListener?.onResponse(response)
            }
        }
    }
    fun queryFlutterString(request: String): String

    /**查询全部数据*/
    fun queryFlutters(requestFilters: RequestFilters?, onResponseAliveListener: OnResponseListener<ResponseAliveFlutters>?) {

        executors.submit {
            val result = queryFluttersString(JsonUtils.toJson(requestFilters))
            val response = Response.parseJson(result, ResponseAliveFlutters::class.java)
            if(response != null){
                onResponseAliveListener?.onResponse(response)
            }
        }
    }

    fun queryFluttersString(request: String): String
}




