package com.hiva.flutter.mvvm.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.TimePicker
import com.hiva.flutter.R
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/5/26
 */
class DateTimeDialog(context: Context) : Dialog(context) {

    private lateinit var mDateDpr: DatePicker
    private lateinit var mTimeTpr: TimePicker
    private lateinit var mCancelBtn: Button
    private lateinit var mConfirmBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_date_time)

        mDateDpr = findViewById(R.id.date_dpr)
        mTimeTpr = findViewById(R.id.time_tpr)
        mCancelBtn = findViewById(R.id.cancel_btn)
        mConfirmBtn = findViewById(R.id.confirm_btn)

        mTimeTpr.setIs24HourView(true)
        mCancelBtn.setOnClickListener(onClickListener)
        mConfirmBtn.setOnClickListener(onClickListener)


        ReminderDateTimeDialog.getSpinner(mDateDpr, "mYearSpinner")
        ReminderDateTimeDialog.getSpinner(mDateDpr, "mMonthSpinner")
        ReminderDateTimeDialog.getSpinner(mDateDpr, "mDaySpinner")
    }


    private val onClickListener =
        View.OnClickListener { v ->
            dismiss()
            if (v === mConfirmBtn) {

                val year = mDateDpr.year
                val month = mDateDpr.month
                val day = mDateDpr.dayOfMonth
                val hour = mTimeTpr.hour
                val minute = mTimeTpr.minute

                calendar[Calendar.YEAR] = year
                calendar[Calendar.MONTH] = month
                calendar[Calendar.DAY_OF_MONTH] = day
                calendar[Calendar.HOUR_OF_DAY] = hour
                calendar[Calendar.MINUTE] = minute
                calendar[Calendar.SECOND] = 0
                calendar[Calendar.MILLISECOND] = 0
                val time = calendar.timeInMillis
                if (this@DateTimeDialog.time != time && onSelectDateTimeListener != null) {
                    onSelectDateTimeListener!!.onSelectDateTime(time)
                }
            }
        }

    private val calendar = Calendar.getInstance()

    private var time: Long = 0
    fun show(time: Long) {
        show()

        this.time = time

        calendar.timeInMillis = time
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val day = calendar[Calendar.DAY_OF_MONTH]
        val hour = calendar[Calendar.HOUR_OF_DAY]
        val minute = calendar[Calendar.MINUTE]

        mDateDpr.init(year, month, day, null)
        mTimeTpr.hour = hour
        mTimeTpr.minute = minute
    }

    interface OnSelectDateTimeListener {
        fun onSelectDateTime(time: Long)
    }

    private var onSelectDateTimeListener: OnSelectDateTimeListener? = null
    fun setOnSelectDateTimeListener(onSelectDateTimeListener: OnSelectDateTimeListener?) {
        this.onSelectDateTimeListener = onSelectDateTimeListener
    }


}