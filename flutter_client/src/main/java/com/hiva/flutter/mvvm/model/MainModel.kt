//package com.hiva.flutter.mvvm.model
//
//import com.hiva.flutter.bean.request.RequestFilters
//import com.hiva.flutter.bean.Flutter
//import com.hiva.flutter.bean.response.ResponseAliveFlutters
//import com.hiva.flutter.bean.Setting
//import com.hiva.flutter.mvvm.model.listener.OnResponseListener
//import com.hiva.flutter.mvvm.model.repository.IRepository
//import com.hiva.flutter.mvvm.model.repository.LocalRepository
//
///**
// *
// * Create By HuangXiangXiang 2022/4/16
// * 实体模型
// *
// */
//class MainModel {
//
//    companion object{
//
//        /**格言数据*/
//        private const val KEY_SETTING_MOTTO = "motto"
//    }
//
//    /**
//     * 数据获取
//     * */
//    private val repository: IRepository = LocalRepository()
//
//    fun queryMotto(onResponseListener: OnResponseListener<Setting>){
//        repository.querySetting(KEY_SETTING_MOTTO, onResponseListener)
//    }
//
//    fun updateMotto(value: String, onResponseListener: OnResponseListener<Boolean>) {
//        repository.insertOrUpdateSetting(Setting(KEY_SETTING_MOTTO, value), onResponseListener)
//    }
//
//    /**编辑*/
//    fun insertOrUpdateFlutter(flutter: Flutter, onResponseListener: OnResponseListener<Boolean>?) {
//        repository.insertOrUpdateFlutter(flutter, onResponseListener)
//    }
//
//    /**查询数据*/
//    fun queryFlutters(requestFilters: RequestFilters?, onResponseAliveListener: OnResponseListener<ResponseAliveFlutters>?) {
//        repository.queryFlutters(requestFilters, onResponseAliveListener)
//    }
//
//
//}
//
//
