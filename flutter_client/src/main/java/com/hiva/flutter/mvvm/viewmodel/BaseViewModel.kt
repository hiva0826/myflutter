package com.hiva.flutter.mvvm.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import com.hiva.flutter.function.ClientFlutterFunction

/**
 *
 * Create By HuangXiangXiang 2022/8/2
 */
open class BaseViewModel : ViewModel() {

    protected lateinit var function: ClientFlutterFunction

    fun init(application : Application){
        function = ClientFlutterFunction.getInstance(application.applicationContext)
    }




}