//package com.hiva.flutter.mvvm.viewmodel
//
//import androidx.lifecycle.MutableLiveData
//import androidx.lifecycle.ViewModel
//import com.hiva.flutter.Response
//import com.hiva.flutter.bean.Flutter
//import com.hiva.flutter.bean.response.ResponseAliveFlutters
//import com.hiva.flutter.bean.Setting
//import com.hiva.flutter.mvvm.model.MainModel
//import com.hiva.flutter.mvvm.model.listener.OnResponseListener
//
///**
// *
// * Create By HuangXiangXiang 2022/4/16
// *
// * 负责完成View与Model间的交互，负责业务逻辑。
// * 生命周期更长
// */
//class MainViewModel: ViewModel() {
//
//
//    var filterValue: FilterValue? = null
//
//    /**模型数据*/
//    private val model = MainModel()
//
//    /**格言 数据*/
//    val motto = MutableLiveData<String>()
//
//
//    /**显示的数据*/
//    val flutters = MutableLiveData<ResponseAliveFlutters>()
//
//
//    /**
//     * 查询 格言
//     * */
//    fun queryMotto() {
//
//        model.queryMotto(object: OnResponseListener<Setting>{
//            override fun onResponse(response: Response<Setting>) {
//                if(response.isSuccess()){
//                    motto.postValue(response.data?.value)
//                }
//            }
//        })
//    }
//
//    /***/
//    fun updateMotto(text: String) {
//
//        model.updateMotto(text, object: OnResponseListener<Boolean>{
//
//            override fun onResponse(response: Response<Boolean>) {
//                if(response.isSuccess() && response.data == true){ // 修改成功
//                    motto.postValue(text)
//                }
//            }
//        })
//    }
//
//
//    /**
//     * 增加或者修改
//     * */
//    fun insertOrUpdateFlutter(flutter: Flutter) {
//        model.insertOrUpdateFlutter(flutter, object: OnResponseListener<Boolean>{
//            override fun onResponse(response: Response<Boolean>) {
//                if(response.isSuccess() && response.data == true){
//                    queryFlutters()// 更新成功 刷新数据
//                }
//            }
//        })
//    }
//
//    /**
//     * */
//    fun queryFlutters() {
//
//        model.queryFlutters(filterValue?.filter, object: OnResponseListener<ResponseAliveFlutters>{
//
//            override fun onResponse(responseAlive: Response<ResponseAliveFlutters>) {
//                if(responseAlive.isSuccess()){
//                    flutters.postValue(responseAlive.data)
//                }
//            }
//        })
//    }
//
//
//
//    override fun onCleared() {
//        super.onCleared()
//
//        filterValue = null
//    }
//
//
//
//
//}