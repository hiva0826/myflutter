package com.hiva.flutter.mvvm.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hiva.flutter.mvvm.view.BaseView
import java.lang.reflect.ParameterizedType

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 */
abstract class BaseActivity<T : BaseView<*,*>> : AppCompatActivity() {

    val view by lazy { initView()} // 仅仅执行一次

    open fun initView():T {

        val tClass = getTClass()
        val con = tClass.getConstructor(AppCompatActivity::class.java)

        return con.newInstance(this)
    }

    private fun getTClass(): Class<T> {
        val genType = javaClass.genericSuperclass
        val params = (genType as ParameterizedType).actualTypeArguments
        return params[0] as Class<T>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view.bind()
        view.onCreate()
    }

    override fun onResume() {
        super.onResume()
        view.onResume()
    }

    override fun onPause() {
        super.onPause()
        view.onPause()
    }

    override fun onStop() {
        super.onStop()
        view.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        view.onDestroy()
    }




}