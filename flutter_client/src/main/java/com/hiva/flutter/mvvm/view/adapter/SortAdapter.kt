package com.hiva.flutter.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hiva.flutter.databinding.AdapterSortBinding
import com.hiva.flutter.bean.Sort
import com.hiva.flutter.bean.Name

/**
 *
 * Create By HuangXiangXiang 2022/5/23
 */
class SortAdapter(context: Context, private val select: Sort): RecyclerView.Adapter<SortAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    var list : List<Name>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    /***/
    private fun selectSort(name: Name){

        if(this.select.name != name){
            select.isUp = true // 默认是升序
            this.select.name = name
        }else{
            select.isUp = !select.isUp
        }
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = AdapterSortBinding.inflate(inflater, parent,false)
        return ViewHolder(binding, onClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        list?.get(position)?.let { holder.setData(it, select)}
    }

    override fun getItemCount(): Int {
        return list?.size?:0
    }

    class ViewHolder(val binding: AdapterSortBinding, onClickListener: View.OnClickListener)
        :RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.tag = this
            binding.root.setOnClickListener(onClickListener)
        }

        lateinit var name: Name
        fun setData(name: Name, select: Sort?){

            this.name = name
            binding.sort.text = name.toString()
            if(name == select?.name){
                binding.sign.visibility = View.VISIBLE
                val sign = if(select.isUp) { "↑" }else{ "↓"}
                binding.sign.text = sign
            }else{
                binding.sign.visibility = View.INVISIBLE
            }
        }
    }

    private val onClickListener = View.OnClickListener{

        val holder = it.tag as ViewHolder
        selectSort(holder.name)
        onSelectListener?.onSelect()
    }

    var onSelectListener: OnSelectListener? = null
    interface OnSelectListener{
        fun onSelect()
    }

}