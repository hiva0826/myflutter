package com.hiva.flutter.mvvm.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import com.hiva.flutter.R
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.databinding.DialogEditFlutterBinding
import com.hiva.flutter.mvvm.view.adapter.LevelAdapter
import com.hiva.flutter.mvvm.view.adapter.StringAdapter
import com.hiva.flutter.utils.DateUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 *
 * Create By HuangXiangXiang 2022/5/19
 */
class EditFlutterDialog(context: Context): Dialog(context, R.style.MyDialog), View.OnClickListener,
    AdapterView.OnItemSelectedListener {


    var onSaveFlutterListener: OnSaveFlutterListener? = null
    interface OnSaveFlutterListener{

        fun onSave(flutter: FlutterDetail)
    }

    private lateinit var flutter: FlutterDetail
    private lateinit var binding: DialogEditFlutterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogEditFlutterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backBtn.setOnClickListener(this)
        binding.completeBtn.setOnClickListener(this)
        binding.endTime.setOnClickListener(this)
        binding.reminderValueTvw.setOnClickListener(this)

        binding.level.adapter = LevelAdapter(context)
        binding.state.adapter = StringAdapter(context, FlutterDetail.State.values())

        binding.reminderTypeSnr.adapter = StringAdapter(context, FlutterDetail.Reminder.Type.values())

        binding.taskTypeSnr.adapter = StringAdapter(context, FlutterDetail.Task.Type.values())
    }

    private var dateTimeDialog: DateTimeDialog? = null
    /**选择时间*/
    private fun showTimePicker(time: Long, onSelectDateTimeListener : DateTimeDialog.OnSelectDateTimeListener){

        dateTimeDialog?: run {
            dateTimeDialog = DateTimeDialog(context)
        }
        dateTimeDialog?.let{

            it.setOnSelectDateTimeListener(onSelectDateTimeListener)
            it.show(time)
        }
    }

    private fun showReminderValue(){

        val type: FlutterDetail.Reminder.Type = FlutterDetail.Reminder.Type.values()[binding.reminderTypeSnr.selectedItemPosition]
        val value: String = binding.reminderValueTvw.text.toString()

        val dialog = ReminderDateTimeDialog(context)
        dialog.setOnSelectDateTimeListener(object : ReminderDateTimeDialog.OnSelectDateTimeListener{
            override fun onSelectDateTime(value: String) {
                binding.reminderValueTvw.text = value
            }
        })

        binding.reminderValueTvw.text = dialog.show(type, value)

    }


    fun show(flutter : FlutterDetail?){
        show()

        this.flutter = flutter?:FlutterDetail(0)

        binding.titleTvw.text = if(this.flutter.id == 0L){
            context.getString(R.string.title_flutter_new)
        }else{
            context.getString(R.string.title_flutter_update)
        }

        binding.createTime.text = this.flutter.createTime?.let {DateUtils.toDateString(it) }

        binding.title.setText(this.flutter.title)
        binding.describe.setText(this.flutter.describe)
        binding.endTime.text = DateUtils.toDateString(this.flutter.createTime)

        binding.level.setSelection(this.flutter.level?:0)
        binding.state.setSelection(FlutterDetail.State.values().indexOf(this.flutter.state))

        binding.reminderTypeSnr.onItemSelectedListener = null
        val position = flutter?.reminder?.reminderType?.ordinal?:0
        binding.reminderTypeSnr.setSelection(position)
        binding.reminderValueTvw.text = flutter?.reminder?.reminderValue

//        Handler().postDelayed({
            binding.reminderTypeSnr.onItemSelectedListener = this
//        }, 500)


        /**唤醒闹钟*/
        val taskPosition = flutter?.task?.taskType?.ordinal?:0
        binding.taskTypeSnr.setSelection(taskPosition)
        binding.taskValueTvw.text = flutter?.task?.taskValue

        window?.let {
            /**
             * 设置宽度全屏，要设置在show的后面
             */
            val layoutParams = it.attributes
            layoutParams.gravity = Gravity.BOTTOM
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
            it.decorView.setPadding(0, 0, 0, 0)
            it.attributes = layoutParams
        }
    }


    override fun onClick(v: View?) {

        when (v) {
            binding.backBtn -> {
                dismiss()
            }
            binding.completeBtn -> {

                flutter.title = binding.title.text.toString()
                if(TextUtils.isEmpty(flutter.title)){
                    binding.title.requestFocus()
                    Toast.makeText(context, "标题不能为空" , Toast.LENGTH_SHORT).show()
                    return
                }

                flutter.describe = binding.describe.text.toString()
//                if(TextUtils.isEmpty(flutter.describe)){
//
//                    Toast.makeText(context, "描述不能为空" , Toast.LENGTH_SHORT).show()
//                    binding.describe.requestFocus()
//                    return
//                }

                flutter.endTime = DateUtils.toTimeLong(binding.endTime.text.toString())
                flutter.level = binding.level.selectedItemPosition
                flutter.state = FlutterDetail.State.values()[binding.state.selectedItemPosition]


                if( flutter.reminder == null){
                    flutter.reminder = FlutterDetail.Reminder()
                }

                flutter.reminder!!.reminderType = FlutterDetail.Reminder.Type.values()[binding.reminderTypeSnr.selectedItemPosition]
                flutter.reminder!!.reminderValue = binding.reminderValueTvw.text.toString()


                onSaveFlutterListener?.onSave(flutter)

                dismiss()


            }
            binding.endTime -> {

                val onSelectDateTimeListener = object : DateTimeDialog.OnSelectDateTimeListener{
                    override fun onSelectDateTime(dateMinute: Long) {

                        flutter.endTime = dateMinute
                        binding.endTime.text = DateUtils.toDateString(flutter.endTime)
                    }
                }
                val end = flutter.endTime ?: System.currentTimeMillis()

                showTimePicker(end, onSelectDateTimeListener)
            }
            binding.reminderValueTvw ->{

                showReminderValue()
            }
        }


    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        showReminderValue()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }


}