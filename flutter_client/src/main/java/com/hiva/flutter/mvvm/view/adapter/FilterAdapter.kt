package com.hiva.flutter.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hiva.flutter.databinding.AdapterFilterBinding
import com.hiva.flutter.bean.Filter
import com.hiva.flutter.utils.TextUtils

/**
 *
 * Create By HuangXiangXiang 2022/5/23
 */
class FilterAdapter(context: Context, val list: MutableList<Filter>): RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    fun addFilter(filter: Filter){

        if(list.add(filter)){
            notifyDataSetChanged()
        }
    }

    private fun deleteFilter(filter: Filter){

        if(list.remove(filter)){
            notifyDataSetChanged()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = AdapterFilterBinding.inflate(inflater, parent,false)
        return ViewHolder(binding, selectListener, deleteListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(
        val binding: AdapterFilterBinding,
        selectListener: View.OnClickListener,
        deleteListener: View.OnClickListener
    ) :RecyclerView.ViewHolder(binding.root){

        init {
            binding.textUtils = TextUtils

            binding.root.tag = this
            binding.root.setOnClickListener(selectListener)
            binding.delete.tag = this
            binding.delete.setOnClickListener(deleteListener)
        }

        fun setData(filter: Filter){
            binding.item = filter
        }
    }

    /**选择*/
    private val selectListener = View.OnClickListener{

        val holder: ViewHolder = it.tag as ViewHolder
        holder.binding.item?.let { f-> onSelectListener?.onSelect(f) }
    }

    /**删除*/
    private val deleteListener = View.OnClickListener{

        val holder: ViewHolder = it.tag as ViewHolder
        val item = holder.binding.item

        item?.let { it2-> deleteFilter(it2) }
    }

    var onSelectListener: OnSelectListener? = null
    interface OnSelectListener{
        fun onSelect(filter: Filter)
    }

}