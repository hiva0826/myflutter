package com.hiva.flutter.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.hiva.flutter.databinding.AdapterLevelBinding

/**
 *
 * Create By HuangXiangXiang 2022/5/26
 */
class LevelAdapter(context: Context): BaseAdapter() {

    private val inflater = LayoutInflater.from(context)

    companion object{
        const val MAX_LEVEL = 10 // 最大的等级级别
    }

    override fun getCount(): Int {
        return MAX_LEVEL
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val holder: ViewHolder
        if(convertView == null){
            val binding = AdapterLevelBinding.inflate(inflater, parent, false)
            holder = ViewHolder(binding)
            binding.root.tag = holder
        }else{
            holder = convertView.tag as ViewHolder
        }
        holder.setData(position)

        return holder.binding.root
    }


    private class ViewHolder(val binding: AdapterLevelBinding) {

        fun setData(position: Int) {
            binding.level.text = position.toString()
        }
    }

}