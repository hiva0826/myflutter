package com.hiva.flutter.mvvm.view.dialog

import android.content.Context
import android.os.Bundle
import android.text.Selection
import android.view.View
import com.hiva.flutter.databinding.DialogInputIntBinding
import com.hiva.flutter.databinding.DialogInputTextBinding

/**
 *
 * Create By HuangXiangXiang 2022/8/4
 */
class InputTextDialog (context: Context): InputBaseDialog(context), View.OnClickListener {

    private lateinit var binding: DialogInputTextBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogInputTextBinding.inflate(layoutInflater )
        binding.cancelBtn.setOnClickListener(this)
        binding.confirmBtn.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onShow(value: String) {

        binding.valueEtt.setText(value)
        Selection.setSelection(binding.valueEtt.text, value.length)
    }

    override fun onClick(v: View?) {

        dismiss()
        if(binding.cancelBtn == v){
            
        }else if(binding.confirmBtn == v){
            onConfirmListener?.onConfirm(binding.valueEtt.text.toString().trim())
        }

    }
}