package com.hiva.flutter.mvvm.view.dialog

import android.content.Context
import android.os.Bundle
import android.view.View
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.databinding.DialogInputStateBinding
import com.hiva.flutter.mvvm.view.adapter.StringAdapter

/**
 *
 * Create By HuangXiangXiang 2022/8/4
 */
class InputStateDialog (context: Context): InputBaseDialog(context), View.OnClickListener {

    private lateinit var binding: DialogInputStateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogInputStateBinding.inflate(layoutInflater)

        val adapter = StringAdapter(context, FlutterDetail.State.values())
        binding.valueSnr.adapter = adapter


        binding.cancelBtn.setOnClickListener(this)
        binding.confirmBtn.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onShow(value: String) {

        val index = try {
            val state = FlutterDetail.State.valueOf(value)
            state.ordinal
        }catch (e : Exception){
            0
        }

        binding.valueSnr.setSelection(index)
    }

    override fun onClick(v: View?) {

        dismiss()
        if(binding.cancelBtn == v){

        }else if(binding.confirmBtn == v){

            val value = FlutterDetail.State.values()[binding.valueSnr.selectedItemPosition].toString()
            onConfirmListener?.onConfirm(value)
        }

    }
}