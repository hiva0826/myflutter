package com.hiva.flutter.mvvm.view.dialog

import android.content.Context
import android.os.Bundle
import android.text.Selection
import android.view.View
import com.hiva.flutter.databinding.DialogInputIntBinding
import com.hiva.flutter.mvvm.view.adapter.LevelAdapter
import java.lang.Exception

/**
 *
 * Create By HuangXiangXiang 2022/8/4
 */
class InputIntDialog (context: Context): InputBaseDialog(context), View.OnClickListener {

    private lateinit var binding: DialogInputIntBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogInputIntBinding.inflate(layoutInflater)

        val adapter = LevelAdapter(context)
        binding.valueSnr.adapter = adapter

        binding.cancelBtn.setOnClickListener(this)
        binding.confirmBtn.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onShow(value: String) {

        val index = try {
            value.toInt()
        }catch (e:Exception){
            0
        }
        binding.valueSnr.setSelection(index)
    }

    override fun onClick(v: View?) {

        dismiss()
        if(binding.cancelBtn == v){

        }else if(binding.confirmBtn == v){
            onConfirmListener?.onConfirm(binding.valueSnr.selectedItemId.toString())
        }

    }
}