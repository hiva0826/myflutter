package com.hiva.flutter.mvvm.model.listener

import com.hiva.flutter.Response

/**
 *
 * Create By HuangXiangXiang 2022/4/28
 */
interface OnResponseListener<T>{

    fun onResponse(response: Response<T>)
}