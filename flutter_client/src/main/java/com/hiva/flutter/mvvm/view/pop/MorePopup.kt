package com.hiva.flutter.mvvm.view.pop

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import com.hiva.flutter.databinding.PopupMoreBinding
import com.hiva.flutter.mvvm.view.activity.QueryActivity
import com.hiva.flutter.utils.AlarmHelper

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 */
class MorePopup(private val context: Context): PopupWindow(), View.OnClickListener {


    private val binding =
//        PopupMoreBinding.inflate(LayoutInflater.from(context),null, false)
        PopupMoreBinding.inflate(LayoutInflater.from(context))

    init {

        binding.refreshTvw.setOnClickListener(this)
        binding.alarmTvw.setOnClickListener(this)
        binding.queryTvw.setOnClickListener(this)
        binding.newTvw.setOnClickListener(this)

        width = ViewGroup.LayoutParams.WRAP_CONTENT
        height = ViewGroup.LayoutParams.WRAP_CONTENT
        isFocusable = true
        isOutsideTouchable = true

        setBackgroundDrawable(ColorDrawable(Color.parseColor("#ffff0000")))

        contentView = binding.root


        AlarmHelper.unReadAlarmSize.observe(context as AppCompatActivity){

            val text = if (it == 0){
                "提醒"
            }else{
                "提醒($it)"
            }
            binding.alarmTvw.text = text
        }

    }


    override fun onClick(v: View?) {

        dismiss()
        if(binding.queryTvw == v){

            val intent = Intent(context, QueryActivity::class.java)
            context.startActivity(intent)

        }else if(binding.alarmTvw == v){
            onFunctionListener?.onAlarm()

        }else if(binding.newTvw == v){
            onFunctionListener?.onNew()

        }else if(binding.refreshTvw == v){
            onFunctionListener?.onRefresh()
        }

    }

    var onFunctionListener : OnFunctionListener? = null
    interface OnFunctionListener{

        fun onNew()
        fun onRefresh()
        fun onAlarm()
    }

}