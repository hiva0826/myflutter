package com.hiva.flutter.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hiva.flutter.bean.Alarm
import com.hiva.flutter.databinding.AdapterFlutterBinding
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.databinding.AdapterAlarmBinding
import com.hiva.flutter.utils.DateUtils
import com.hiva.flutter.utils.TextUtils

/**
 *
 * Create By HuangXiangXiang 2022/5/23
 */
class AlarmAdapter(context: Context): RecyclerView.Adapter<AlarmAdapter.FlutterViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    var list : List<Alarm>? = null
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlutterViewHolder {

//        val binding = AdapterFlutterBinding.inflate(inflater) // 使用这个宽高无效
        val binding = AdapterAlarmBinding.inflate(inflater, parent,false)

        return FlutterViewHolder(binding, onClickListener)
    }

    override fun onBindViewHolder(holder: FlutterViewHolder, position: Int) {

        list?.get(position)?.let { holder.setData(it) }
    }

    override fun getItemCount(): Int {
        return list?. size ?: 0
    }

    class FlutterViewHolder(val binding: AdapterAlarmBinding, onClickListener: View.OnClickListener) :RecyclerView.ViewHolder(binding.root){

        init {
            binding.textUtils = TextUtils

            binding.root.tag = this
            binding.root.setOnClickListener(onClickListener)
        }

        fun setData(alarm: Alarm){
            binding.alarm = alarm
        }

    }

    private val onClickListener = View.OnClickListener{

        val holder: FlutterViewHolder = it.tag as FlutterViewHolder
        holder.binding.alarm?.let { f-> onSelectListener?.onSelect(f) }
    }


    var onSelectListener: OnSelectListener? = null
    interface OnSelectListener{
        fun onSelect(alarm: Alarm)
    }

}