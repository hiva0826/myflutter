package com.hiva.flutter.mvvm.view

import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.hiva.flutter.mvvm.viewmodel.BaseViewModel
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 */
abstract class BaseView<B: ViewDataBinding, M: BaseViewModel>(val activity: AppCompatActivity)  {

    lateinit var binding: B
    lateinit var viewModel: M

    fun bind(){

        val genType = javaClass.genericSuperclass
        val params = (genType as ParameterizedType).actualTypeArguments

        val bClass: Class<B> = params[0] as Class<B>
        val mClass: Class<M> = params[1] as Class<M>

        binding  = inflate(bClass, LayoutInflater.from(activity))

        val data = bClass.getDeclaredField("mView")
        data.isAccessible = true
        data.set(binding, this)

//        viewModel = ViewModelProvider(activity).get(mClass)
        viewModel = ViewModelProvider(activity).get(mClass)
        viewModel.init(activity.application)

        activity.setContentView(binding.root) // 设置数据
    }



    private fun getInflateMethod(bClass: Class<B>): Method {
        return bClass.getMethod("inflate", LayoutInflater::class.java)
    }

    private fun inflate(bClass: Class<B>, inflater: LayoutInflater):B{
        val method = getInflateMethod(bClass)
        return method.invoke(null, inflater) as B
    }





    open fun onCreate(){}

    open fun onResume(){}

    open fun onPause(){}

    open fun onStop(){}

    open fun onDestroy(){}


    open fun onStartData(){}

    open fun onStopData(){}


    fun back(view: View){

        activity.onBackPressed()
    }

}