package com.hiva.flutter.mvvm.view.dialog

import android.app.Dialog
import android.content.Context

/**
 *
 * Create By HuangXiangXiang 2022/8/4
 */
abstract class InputBaseDialog (context: Context): Dialog(context){

    interface OnConfirmListener{
        fun onConfirm(value: String)
    }

    protected var value : String? = null
    protected var onConfirmListener: OnConfirmListener? = null

    /**显示*/
    fun show(value : String, onConfirmListener: OnConfirmListener){

        this.value = value
        this.onConfirmListener = onConfirmListener
        show()
        onShow(value)
    }

    protected abstract fun onShow(value : String)


}