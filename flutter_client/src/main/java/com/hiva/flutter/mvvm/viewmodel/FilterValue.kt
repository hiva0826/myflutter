package com.hiva.flutter.mvvm.viewmodel

import android.content.Context
import com.hiva.flutter.bean.Filter
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.Sort
import com.hiva.helper.json.JsonUtils

/**
 *
 * Create By HuangXiangXiang 2022/5/30
 */
class FilterValue(val context: Context) {


    companion object{

        private const val NAME_OBJECT = "object"
        private const val KEY_FILTERS = "filters"
    }


    val parameterNames = null
    val filterTypes = Filter.Type.values()

    val filter: RequestFilters = readFilters()


    /**读取数据*/
    private fun readFilters(): RequestFilters {

        val sp = context.getSharedPreferences(NAME_OBJECT, Context.MODE_PRIVATE)
        val json = sp.getString(KEY_FILTERS, null)

        var requestFilters : RequestFilters? = null
        try {
            requestFilters = JsonUtils.parseJson(json, RequestFilters::class.java)
        }catch (e: Exception){
        }
        if(requestFilters == null){
            requestFilters = RequestFilters()
        }
        return requestFilters
    }

    /**保存数据*/
    private fun writeFilters(filter: RequestFilters){

        val sp = context.getSharedPreferences(NAME_OBJECT, Context.MODE_PRIVATE)
        val editor = sp.edit()
        val json = JsonUtils.toJson(filter)
        editor.putString(KEY_FILTERS, json)
        editor.apply()
    }


    /**修改排序*/
    fun getSort(): Sort {
        return filter.sort
    }

    /**
     * 获取临时数据
     * */
    fun getFilters(): MutableList<Filter>{

        return filter.list
    }

    /**重置数据*/
    fun reset(){

        filter.sort.name = null
        filter.list.clear()

        writeFilters(filter)
    }

    /**
     * 提交 (临时转换成正式)
     * */
    fun confirm(){

        writeFilters(filter)
    }


}