//package com.hiva.flutter.mvvm.model.repository
//
//import com.hiva.flutter.mvvm.FlutterApplication
//import com.hiva.flutter.server.DBHelper
//
///**
// *
// * Create By HuangXiangXiang 2022/4/28
// *
// * 本地数据
// */
//class LocalRepository : IRepository {
//
//    private val dbHelper = DBHelper.getInstance(FlutterApplication.context)
//    override fun insertOrUpdateSettingString(request: String): String {
//        return dbHelper.insertOrUpdateSetting(request)
//    }
//
//    override fun deleteSettingString(request: String): String {
//        return dbHelper.deleteSetting(request)
//    }
//
//    override fun querySettingString(request: String): String {
//        return dbHelper.querySetting(request)
//    }
//
//    override fun insertOrUpdateFlutterString(request: String): String {
//        return dbHelper.insertOrUpdateFlutter(request)
//    }
//
//    override fun deleteFlutterString(request: String): String {
//        return dbHelper.deleteFlutter(request)
//    }
//
//    override fun queryFlutterString(request: String): String {
//        return dbHelper.queryFlutter(request)
//    }
//
//    override fun queryFluttersString(request: String): String {
//        return dbHelper.queryFlutters(request)
//    }
//
//
//}