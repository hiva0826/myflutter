package com.hiva.flutter.mvvm.view

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.hiva.flutter.bean.Flutter
import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.response.ResponseFlutterDetail
import com.hiva.flutter.databinding.ActivityFirstBinding
import com.hiva.flutter.listener.OnResponseListener
import com.hiva.flutter.mvvm.view.activity.AlarmActivity
import com.hiva.flutter.mvvm.view.adapter.FlutterAdapter
import com.hiva.flutter.mvvm.view.dialog.EditFlutterDialog
import com.hiva.flutter.mvvm.view.pop.MorePopup
import com.hiva.flutter.mvvm.viewmodel.FirstViewModel
import com.hiva.flutter.utils.AlarmHelper
import com.hiva.helper.app.MainThreadUtils

/**
 *
 * Create By HuangXiangXiang 2022/7/20
 *
 * view 脱离 activity 层
 *
 */
class FirstView (activity: AppCompatActivity) : BaseView<ActivityFirstBinding, FirstViewModel >(activity){


    override fun onCreate() {
        super.onCreate()

        val adapter = FlutterAdapter(activity)
        adapter.onSelectListener = object :  FlutterAdapter.OnSelectListener {

            override fun onSelect(flutter: Flutter) {

                viewModel.queryDetailFlutter(flutter.id, object :
                    OnResponseListener<ResponseFlutterDetail> {
                    override fun onSuccess(t: ResponseFlutterDetail) {

                        MainThreadUtils.executeInMain{
                            show(t.flutterDetail)
                        }
                    }

                    override fun onFail(code: Int, message: String?) {
                    }
                })
            }
        }
        binding.aliveFlutterRvw.layoutManager = LinearLayoutManager(activity)
        binding.aliveFlutterRvw.adapter = adapter
        viewModel.aliveFlutters.observe(activity, {
            adapter.list = it
        })

        viewModel.queryAliveFlutters()
        AlarmHelper.unReadAlarmSize.observe(activity){

            val text = if (it == 0){
                 ":"
            }else{
                 ":($it)"
            }
            binding.moreBtn.text = text
        }

        viewModel.startAliveFlutterChangedListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.stopAliveFlutterChangedListener()
    }


    private var morePopup : MorePopup? = null

    /**更多功能*/
    fun more(view: View){

        if(morePopup == null){
            morePopup = MorePopup(activity)
            morePopup?.onFunctionListener =object : MorePopup.OnFunctionListener {
                override fun onNew() {
                    show(null)
                }

                override fun onRefresh() {
                    viewModel.queryAliveFlutters()
                }

                override fun onAlarm() {

                    AlarmActivity.startActivity(activity)
                }
            }
        }
        morePopup?.showAsDropDown(view)
    }

    private var editFlutterDialog: EditFlutterDialog? = null
    private fun show(flutter: FlutterDetail?){

        if(editFlutterDialog == null){
            editFlutterDialog = EditFlutterDialog(activity)
            editFlutterDialog!!.onSaveFlutterListener = object : EditFlutterDialog.OnSaveFlutterListener{
                override fun onSave(flutter: FlutterDetail) {
                    // 保存（数据）
                    viewModel.insertOrUpdateFlutter(flutter)
                }
            }
        }
        editFlutterDialog!!.show(flutter)
    }



}