package com.hiva.flutter.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.hiva.flutter.databinding.AdapterStringBinding

/**
 *
 * Create By HuangXiangXiang 2022/5/26
 */
class StringAdapter(context: Context, list: List<*>? = null) : BaseAdapter() {
    constructor(context: Context, arrays: Array<*>) : this(context, listOf(*arrays))

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    var list: List<*>? = list
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    fun indexOf(any: Any):Int{

        list?.let {
            for(i in it.indices){
                if(any.toString() == it[i].toString()){
                    return i
                }
            }
        }
        return 0

    }



    override fun getCount(): Int {

        return list?.size?:0
    }
    override fun getItem(position: Int): Any {
        return list?.get(position)!!
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val holder: ViewHolder
        if(convertView == null){
            val binding = AdapterStringBinding.inflate(inflater, parent, false)

//            binding.lifecycleOwner = context as AppCompatActivity

            holder = ViewHolder(binding)
            binding.root.tag = holder
        }else{
            holder = convertView.tag as ViewHolder
        }
        holder.setData(list?.get(position))

        return holder.binding.root
    }


    private class ViewHolder(val binding: AdapterStringBinding) {

        fun setData(any: Any?) {
//            binding.obj = any
            binding.text.text = any.toString()
        }
    }

}