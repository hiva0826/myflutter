package com.hiva.flutter.listener

import java.lang.reflect.ParameterizedType

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
interface OnResponseListener<T> {

    /**成功响应*/
    fun onSuccess(t: T)

    /**失败响应*/
    fun onFail(code:Int, message: String? )

    /**获得T的类*/
    fun getTClass(): Class<T> {
        val genType = javaClass.genericInterfaces[0]
        val params = (genType as ParameterizedType).actualTypeArguments
        return params[0] as Class<T>
    }

//    private fun getTClass(): Class<T> {
//        val genType = javaClass.genericSuperclass
//        val params = (genType as ParameterizedType).actualTypeArguments
//        return params[0] as Class<T>
//    }
}