package com.hiva.flutter.bean.response

import com.hiva.flutter.bean.Alarm

/**
 *
 * Create By HuangXiangXiang 2022/7/16
 */
data class ResponseChangedAlarm (val alarm: Alarm)