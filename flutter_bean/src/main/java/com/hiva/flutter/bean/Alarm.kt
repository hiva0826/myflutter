package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/8/9
 */
data class Alarm(

    var id: Long  ,         // id
    var fId: Long ,         // naox
    var time: Long ,        // 提醒时间
    var title: String  ,   // 提醒标题
    var info: String ,      // 提醒信息
    var isRead : Boolean,   // 是否已看
){


}