package com.hiva.flutter.bean.response

import com.hiva.flutter.bean.Name


/**
 *
 * Create By HuangXiangXiang 2022/7/12
 */
data class ResponseSortNames(val sorts: List<Name>)