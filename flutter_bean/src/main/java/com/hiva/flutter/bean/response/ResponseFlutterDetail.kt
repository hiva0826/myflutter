package com.hiva.flutter.bean.response

import com.hiva.flutter.bean.FlutterDetail

/**
 *
 * Create By HuangXiangXiang 2022/7/12
 * 详细代办事项
 */
data class ResponseFlutterDetail(
    val flutterDetail: FlutterDetail?
)