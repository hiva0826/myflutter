package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/5/27
 */
data class Filter(
    val name: Name,         // 名称
    val type: Type,         // 类型
    val value: String,      // 参数
) {


    enum class Type(private val alias: String) {

        LESS("小于"),      // 小于
        MORE("大于"),      // 小于
        EQUAL("等于"),      // 等于
        NOT_EQUAL("不等于"),      // 不等于

        LIKE("包含"),         // 包含
        ;

        override fun toString(): String {
            return alias
        }
    }




}