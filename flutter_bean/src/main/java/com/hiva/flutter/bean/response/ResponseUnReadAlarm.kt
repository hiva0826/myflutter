package com.hiva.flutter.bean.response

import com.hiva.flutter.bean.Alarm

/**
 *
 * Create By HuangXiangXiang 2022/8/9
 */
data class ResponseUnReadAlarm(
    val alarms: List<Alarm>
)
