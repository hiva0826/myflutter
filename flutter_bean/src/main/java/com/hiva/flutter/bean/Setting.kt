package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/5/18
 */
data class Setting(
    var key: String,
    var value: String?
    )