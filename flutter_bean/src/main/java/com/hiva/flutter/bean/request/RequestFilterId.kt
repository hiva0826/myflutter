package com.hiva.flutter.bean.request

/**
 *
 * Create By HuangXiangXiang 2022/4/29
 */
data class RequestFilterId(
    var id: Long = 0,
)