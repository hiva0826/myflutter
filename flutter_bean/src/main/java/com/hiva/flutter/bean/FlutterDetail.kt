package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/7/19
 * 代办事项（详细）
 */
data class FlutterDetail(

    var id: Long = 0,                           // id
    var createTime: Long? = null,               // 创建时间
    var title: String = "",                     // 标题
    var describe: String?= null,                // 描述
    var level: Int? = 0 ,                     // 重要等级（数字越大 越重要 默认为0）
    var state: State?= null,                    // 状态
    var endTime: Long? = null,                   // 结束时间
    var reminder: Reminder?= null,              // 提醒信息
    var task: Task?= null,                      // 执行信息

){

    /**
     * 任务状态
     *  计划中
     *  任务中
     *  已完成
     *  已放弃
     *  其他
     */
    enum class State(private val alias: String) {

        TASK("进行"),       // 进行
        PLAN("计划"),       // 计划
        COMPLETED("完成"),  // 完成
        ABANDONED("放弃"),  // 放弃
        ;

        override fun toString(): String {
            return alias
        }
    }

    /**
     *
     * Create By HuangXiangXiang 2022/4/13
     * 提醒时间
     * 0、固定时间 X月X日
     * 1、每年 X月X日
     * 2、每月 X日
     * 3、每天 具体时间
     * 4、每周 周几
     * 5、每隔一段时间 具体时间
     *
     */
    data class Reminder(
        var reminderType: Type = Type.FIXED,     // 定时的类型
        var reminderValue: String? = null,  // 数据
    ) {

        companion object{

            const val PATTERN_FIXED = "yyyy-MM-dd HH:mm:ss"
            const val PATTERN_YEAR = "MM-dd HH:mm:ss"
            const val PATTERN_MONTH = "dd HH:mm:ss"
            const val PATTERN_DAY = "HH:mm:ss"
            const val PATTERN_WEEK = "HH:mm:ss"
            const val PATTERN_BIRTH_DAY = "MM-dd"
            const val PATTERN_HOLIDAYS = "HH:mm:ss"
            const val PATTERN_NON_HOLIDAYS = "HH:mm:ss"
            const val PATTERN_CYCLE = "yyyy-MM-dd HH:mm:ss"

            const val SPLIT = "|"
        }

        /**提醒类型*/
        enum class Type(private val alias: String, val pattern: String){

            FIXED("固定", PATTERN_FIXED),                   // 固定    （2022-07-18 14:41:22）
            YEAR("每年", PATTERN_YEAR),                     // 每年    （07-18 14:41:22）
            MONTH("每月", PATTERN_MONTH),                   // 每月    （18 14:41:22）
            DAY("每天",PATTERN_DAY),                        // 每天    （14:41:22）
            WEEK("每周",PATTERN_WEEK),                      // 每周     （0000000|14:41:22）
            BIRTH_DAY("生日",PATTERN_BIRTH_DAY),            // 生日    （07-18）
            HOLIDAYS("节假日",PATTERN_HOLIDAYS),            // 节假日   （14:41:22）
            NON_HOLIDAYS("非节假日",PATTERN_NON_HOLIDAYS),  // 非节假日（14:41:22）
            CYCLE("循环",PATTERN_CYCLE),                   // 循环   （6000|2022-07-18 14:41:22）
            ;

            override fun toString(): String {
                return alias
            }
        }

    }

    data class Task(
        var taskType: Type? = null,     // 任务类型
        var taskValue: String? = null,  // 任务数据
    ){
        enum class Type(private val alias: String){ //

            NORMAL("常规"),
            ;

            override fun toString(): String {
                return alias
            }
        }
    }


}