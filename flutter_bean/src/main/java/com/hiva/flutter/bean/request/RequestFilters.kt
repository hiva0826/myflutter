package com.hiva.flutter.bean.request

import com.hiva.flutter.bean.Filter
import com.hiva.flutter.bean.Sort

/**
 *
 * Create By HuangXiangXiang 2022/4/29
 */
data class RequestFilters(
    val sort: Sort = Sort(),    // 排序
    val list: MutableList<Filter> = ArrayList(),    // 过滤条件
)