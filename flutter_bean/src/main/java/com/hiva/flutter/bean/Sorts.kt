package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/7/12
 */
data class Sorts(val sorts: List<Sort>)