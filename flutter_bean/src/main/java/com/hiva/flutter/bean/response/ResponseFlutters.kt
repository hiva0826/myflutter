package com.hiva.flutter.bean.response

import com.hiva.flutter.bean.FlutterDetail

/**
 *
 * Create By HuangXiangXiang 2022/5/23
 */
data class ResponseFlutters(
    val flutters: List<FlutterDetail>
    )
