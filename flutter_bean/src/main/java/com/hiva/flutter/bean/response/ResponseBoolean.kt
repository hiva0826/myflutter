package com.hiva.flutter.bean.response

/**
 *
 * Create By HuangXiangXiang 2022/7/12
 */
data class ResponseBoolean(val result: Boolean)