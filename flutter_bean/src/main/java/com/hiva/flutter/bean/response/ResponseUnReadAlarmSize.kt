package com.hiva.flutter.bean.response

/**
 *
 * Create By HuangXiangXiang 2022/8/9
 */
data class ResponseUnReadAlarmSize(
    val size: Int
)
