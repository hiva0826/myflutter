package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/4/16
 */
data class Schedule(

    var id: Long = 0,
    var tid: Long = 0,   // 任务id
    /**更新时间*/
    var updateTime: Long? = null
)
