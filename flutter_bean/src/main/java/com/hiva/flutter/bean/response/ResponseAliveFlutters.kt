package com.hiva.flutter.bean.response

import com.hiva.flutter.bean.Flutter

/**
 *
 * Create By HuangXiangXiang 2022/5/23
 */
data class ResponseAliveFlutters(
    val flutters: List<Flutter>
    )
