package com.hiva.flutter.bean

/**
 * Create By HuangXiangXiang 2022/5/27
 */
data class Name(
    val type: String,   // 类型
    val alias: String   // 显示别称
    ) {

    override fun toString(): String {
        return alias
    }
}