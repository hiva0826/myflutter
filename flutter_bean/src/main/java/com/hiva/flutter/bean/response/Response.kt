package com.hiva.flutter.bean.response

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
class Response<T> {

    companion object{

        const val CODE_SUCCESS = 0
        const val CODE_INNER = -1 // 内部异常


        /***/
        fun <T> success(data: T): Response<T>{

            val response  = Response<T>()
            response.data = data

            return response
        }


        fun <T> fail(code: Int, message: String?): Response<T>{

            val response  = Response<T>()
            response.code = code
            response.message = message

            return response
        }

    }

    var code: Int = CODE_SUCCESS
    var message: String? = null
    var data: T? = null


    fun isSuccess(): Boolean {
        return code == CODE_SUCCESS
    }

}