package com.hiva.flutter.bean


/**
 * Create By HuangXiangXiang 2022/2/14
 *  代办事项(概略)
 */
data class Flutter(

    var id: Long = 0,                           // id
//    var createTime: Long? = null,               // 创建时间
    var title: String? = null,                  // 事情名称
//    var describe: String?= null,                // 事情的描述
//    var startTime: Long?= null,                 // 起始时间
    var level: Int? = 0,                     // 重要等级（数字越大 越重要 默认为0）
//    var state: State?= null,                    // 状态
    var endTime: Long? = 0,                   // 结束时间
//    var reminder: Reminder?= null,              // 提醒信息
//    var task: Task?= null,                      // 执行信息
)  {






}