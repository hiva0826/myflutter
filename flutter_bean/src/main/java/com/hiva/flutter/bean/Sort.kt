package com.hiva.flutter.bean

/**
 *
 * Create By HuangXiangXiang 2022/5/27
 */
/**排序*/
data class Sort(
    var name: Name? = null,
    var isUp: Boolean = true
){

    override fun toString(): String {
        return name.toString()
    }

}
