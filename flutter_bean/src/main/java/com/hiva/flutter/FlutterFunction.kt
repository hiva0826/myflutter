package com.hiva.flutter

import com.hiva.flutter.bean.FlutterDetail
import com.hiva.flutter.bean.request.RequestFilters
import com.hiva.flutter.bean.response.*
import com.hiva.flutter.listener.OnResponseListener

/**
 *
 * Create By HuangXiangXiang 2022/7/12
 */
interface FlutterFunction {


    /**
     * 监听活跃的数据变化
     * */
    fun setAliveFlutterChangedListener(listener: OnResponseListener<ResponseChangedAliveFlutter>?)
    /**
     * 、查询活跃的数据
     * */
    fun queryAliveFlutters(listener: OnResponseListener<ResponseAliveFlutters>)

    /**
     * 获得 全部可以排序的列表
     * */
    fun querySortNames(listener: OnResponseListener<ResponseSortNames>)

    /**
     * 获得 全部可以过滤的列表
     * */
    fun queryFilterNames(listener: OnResponseListener<ResponseFilterNames>)


    /**、根据 排序 和过滤 列表 条件查询 代办事项 */
    fun queryFlutters(requestFilters: RequestFilters, listener: OnResponseListener<ResponseFlutters> )


    /**、新增/或者修改一条代表事项*/
    fun insertOrUpdateFlutter(flutterDetail: FlutterDetail, listener: OnResponseListener<ResponseBoolean>)

    /**、删除一条代表事项*/
    fun deleteFlutter(id: Long, listener: OnResponseListener<ResponseBoolean>)

    /**、查看一条代表详细事项*/
    fun queryDetailFlutter(id: Long, listener: OnResponseListener<ResponseFlutterDetail>)



    /**
     * 监听闹钟变化
     * */
    fun setAlarmChangedListener(listener: OnResponseListener<ResponseChangedAlarm>?)

    /** 查看全部已读提醒*/
    fun queryUnReadAlarm(listener: OnResponseListener<ResponseUnReadAlarm>)

    /** 查看全部已读提醒*/
    fun queryUnReadAlarmSize(listener: OnResponseListener<ResponseUnReadAlarmSize>)

    /** 已读提醒*/
    fun readAlarm(id: Long, listener: OnResponseListener<ResponseBoolean>)

    /** 全部已读提醒*/
    fun readAllAlarm(listener: OnResponseListener<ResponseBoolean>)

}