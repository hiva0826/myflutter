package com.hiva.flutter.utils

import java.io.*
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/8/8
 */
object HolidaysUtils {

    /**日期的格式*/
    private const val PATTERN_DATE = "yyyy-MM-dd"
    private val FORMAT = SimpleDateFormat(PATTERN_DATE, Locale.CHINA)

    private const val PATH_HOLIDAYS   = "/sdcard/holidays/special.txt"

    /**0表示放假*/
    private const val RESULT_TRUE   = 0

    private val specialHolidaysMap = HashMap<String, Boolean>()

    /**
     * 更新数据
     * */
    fun updateSpecialHolidays() {

        updateSpecialHolidays(PATH_HOLIDAYS)
    }

    fun updateSpecialHolidays(path : String) {

        specialHolidaysMap.clear()

        File(path).readLines().forEach {

            try {
                val values = it.split(" ").toTypedArray()

                val time = values[0]
                val date = FORMAT.parse(time) // 将数据格式化
                val key = FORMAT.format(date)

                val value = values[1].toInt() == RESULT_TRUE

                specialHolidaysMap[key] = value

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    init {
        updateSpecialHolidays()
    }


    /**是否是节假日*/
    fun isHolidays(date: Date):Boolean {

        val time = FORMAT.format(date)
        val result = specialHolidaysMap[time]

        result?.let {
            return result
        }?: run {
            return isBaseHolidays(date)
        }

    }


    /**
     * 是否是节假日
     * 简单的（周六和周日）
     * */
    private fun isBaseHolidays(date: Date):Boolean {

        val calendar = Calendar.getInstance()
        calendar.time = date

        val week = calendar.get(Calendar.DAY_OF_WEEK)
        return Calendar.SUNDAY == week || Calendar.SATURDAY == week
    }


}