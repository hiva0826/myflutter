package com.hiva.flutter.utils

import android.util.SparseIntArray
import java.util.*

/**
 *
 * Create By HuangXiangXiang 2022/8/6
 */
object WeekUtils {

    const val FLAG_SUNDAY      = 0b1000000  //64
    const val FLAG_MONDAY      = 0b0100000  //32
    const val FLAG_TUESDAY     = 0b0010000  //16
    const val FLAG_WEDNESDAY   = 0b0001000  //8
    const val FLAG_THURSDAY    = 0b0000100  //4
    const val FLAG_FRIDAY      = 0b0000010  //2
    const val FLAG_SATURDAY    = 0b0000001  //1


    private val WEEK = SparseIntArray()
    init {
        WEEK.put(Calendar.SUNDAY, FLAG_SUNDAY   )
        WEEK.put(Calendar.MONDAY, FLAG_MONDAY   )
        WEEK.put(Calendar.TUESDAY, FLAG_TUESDAY  )
        WEEK.put(Calendar.WEDNESDAY, FLAG_WEDNESDAY)
        WEEK.put(Calendar.THURSDAY, FLAG_THURSDAY )
        WEEK.put(Calendar.FRIDAY, FLAG_FRIDAY   )
        WEEK.put(Calendar.SATURDAY, FLAG_SATURDAY )
    }


    fun getDayOfWeek(calendar: Calendar ) : Int{

        val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        return WEEK.get(dayOfWeek)
    }





    /*********************************以下为一个帮助类的方法 */
    /**
     * 切换选择星期
     * @param checkWeeks 当前选择的星期数据
     * @param flag 算需要切换的星期
     *
     */
    fun switchWeek(checkWeeks: Int, flag: Int): Int {
        return checkWeeks xor flag
    }


    /**
     * 是否包含
     */
    fun isContains(checkWeeks: Int, flag: Int): Boolean {
        return checkWeeks and flag == flag
    }


}