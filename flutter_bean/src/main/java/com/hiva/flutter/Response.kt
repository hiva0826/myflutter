package com.hiva.flutter

import android.text.TextUtils
import com.hiva.helper.json.JsonUtils

/**
 *
 * Create By HuangXiangXiang 2022/5/12
 *
 * 响应数据
 */
class Response<T> {

    companion object{

        /**成功码*/
        const val CODE_SUCCESS = 0

        fun <T> parseJson(json: String?, tClass: Class<T>?): Response<T>? {
            if (TextUtils.isEmpty(json)) {
                return null
            }
            val resp = JsonUtils.parseJson(json, Response::class.java)
            if(resp === null){
                return null
            }
            val response: Response<T> = resp as Response<T>

            var dataJson: String? = null
            if (response.data != null) {
                dataJson = JsonUtils.toJson(response.data) // 转换回json数据
            }
            if (tClass != null && dataJson != null) {
                val data = JsonUtils.parseJson(dataJson, tClass)
                response.data = data
            }
            return response
        }
    }


    var code: Int      // 成功 为0
    var msg: String?
    var data: T?

    constructor(code: Int, msg: String?, data: T?) {
        this.code = code
        this.msg = msg
        this.data = data
    }

    /**正常时候*/
    constructor(data: T?):  this(CODE_SUCCESS, null, data)

    /**错误时候*/
    constructor(code: Int, msg: String?):  this(code, msg, null)


    fun isSuccess(): Boolean{
        return code == CODE_SUCCESS
    }


    fun toJson(): String{

        return JsonUtils.toJson(this)
    }
}