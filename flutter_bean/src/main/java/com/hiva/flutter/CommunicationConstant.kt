package com.hiva.flutter

/**
 *
 * Create By HuangXiangXiang 2022/7/14
 */
object CommunicationConstant {




    /**活跃的数据变化**/
    const val TOPIC_ALIVE_FLUTTERS                  = "aliveFlutters"

    /**查询活跃的数据*/
    const val REQUEST_QUERY_ALIVE_FLUTTERS          = "queryAliveFlutters"
    /**获得 全部可以排序的列表*/
    const val REQUEST_QUERY_SORT_NAMES              = "querySortNames"
    /**获得 全部可以过滤的列表*/
    const val REQUEST_QUERY_FILTER_NAMES            = "queryFilterNames"
    /**根据 排序 和过滤 列表 条件查询 代办事项 */
    const val REQUEST_QUERY_FLUTTERS                = "queryFlutters"
    /**新增/或者修改一条代表事项*/
    const val REQUEST_INSERT_OR_UPDATE_FLUTTER      = "insertOrUpdateFlutter"
    /**删除一条代表事项*/
    const val REQUEST_DELETE_FLUTTER                = "deleteFlutter"
    /**查看一条代表详细事项*/
    const val REQUEST_QUERY_DETAIL_FLUTTER          = "queryDetailFlutter"



    /**闹钟数据**/
    const val TOPIC_ALARM                           = "alarm"

    /**查询全部没有读取的警报*/
    const val REQUEST_QUERY_UN_READ_ALARM           = "queryUnReadAlarm"
    /**查询全部没有读取的数量*/
    const val REQUEST_QUERY_UN_READ_ALARM_SIZE      = "queryUnReadAlarmSize"
    /**读取的警报*/
    const val REQUEST_READ_ALARM                    = "readAlarm"
    /**读取的警报*/
    const val REQUEST_READ_ALL_ALARM                = "readAllAlarm"




}